

def stem(word1, word2):
    if abs(len(word1) - len(word2)) > 2:
        return None, None

    different_letter = False

    len_shorter_word = min(len(word1), len(word2))

    # If the words are related, we assume that the shorter word will have a suffix of length between 0 and 2 letters
    # and the root word will have at least 3 letters (hence the last index is would be 2)
    min_end_of_root = max(len_shorter_word-2, 2)

    for index in range(len_shorter_word):
        if word1[index] != word2[index]:
            different_letter = True
            break

    # These 2 words do not share a root
    if index < min_end_of_root:
        return None, None

    # In case the root word is equal to one of the words, such as in stem('box', 'boxes'), we need to prevent the
    # last letter (x) from being chopped off
    if not different_letter:
        index +=1

    # Split each word in the root and suffix
    root_word = word1[:index]
    suffix1 = word1[index:]
    suffix2 = word2[index:]
    # suffixes = [s for s in [suffix1, suffix2] if s != '']
    return root_word, [suffix1, suffix2]#suffixes
#
# print(stem('box', 'boxes'))
# print(stem('boxes','box'))
# print(stem('roja', 'rojo'))
# print(stem('is', 'issue'))
#


