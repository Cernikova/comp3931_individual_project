import pickle
import re
from pickle_functions import read_sentences
import logging
from constants import DATA_FOLDER

logging.basicConfig(format='%(message)s', level=logging.INFO)

def find_all_tokens(rcl):
    p = re.compile(r'\((?P<key>\w+): (?P<value>\w+)\)')
    m = p.findall(rcl)
    return m

def trees_to_dict(rcl):
    tree= {}
    rcl_new = rcl
    count = 0

    rm = re.compile(r'\(\w+:\s+\(token:\s+[0-9]+\s*[0-9]*\)\)')
    empty_tokens = rm.findall(rcl_new)
    if empty_tokens:
        for empty_token in empty_tokens:
            logging.warning('missing element: '+ str(empty_token))
            rcl_new = re.sub(re.escape(empty_token), '', rcl_new)

    while True:
        p = re.compile(r'\((?P<key>\w+): (?P<value>\w+)\s+\(token:\s+[0-9]+\s*[0-9]*\)\)')
        m = re.search(p, rcl_new)
        if not m:
            if 'token' in rcl_new:
                logging.info('PROBLEM: '+ rcl_new)
            break
        else:
            remove = m.group(0)
            rcl_new = re.sub(re.escape(remove), str(count), rcl_new)
            tree[count] = {}
            tree[count][m.groupdict()['key']] = m.groupdict()['value']
            count +=1
            logging.debug('REMOVE: ' + remove)
            logging.debug('NEW: ' + rcl_new)
    logging.debug(tree)

    while True:
        p = re.compile(r'\((?P<key>\w+[-]*\w+):\s+(?P<value>[0-9]+\s*[0-9]*\s*[0-9]*)\)')
        m = re.search(p, rcl_new)
        if not m:
            logging.info("nothing found in: " + rcl_new)
            break
        else:
            remove = m.group(0)
            rcl_new = re.sub(re.escape(remove), str(count), rcl_new)
            tree[count] = {}

            element_name = m.groupdict()['key']
            tree[count][element_name] = {}
            for index in m.groupdict()['value'].split(' '):
                tree[count][element_name].update(tree[int(index)])

            count += 1
            logging.debug('REMOVE: ' + remove)
            logging.debug('NEW: ' + rcl_new)
    event = max(tree.keys())
    logging.info(tree[event])
    return tree[event]



def find_atomic_elements(rcl):
    p = re.compile(r'\((?P<key>\w+): (?P<value>\w+[-]*\w+)\s+\(token:\s+[0-9]+\s*[0-9]*\)\)')
    m = p.findall(rcl)
    # logging.info(m)
    return m

def find_rcl_element(expression):
    p = re.compile(r'\((?P<key>\w+): (?P<value>\w+)\)')
    m = p.search(expression)
    if m:
        return m.groupdict()
    else:
        return None

def correct_entity_types(atomic_elements):
    unknown_types = ['reference', 'board', 'position', 'tile', 'type-reference']
    location_types = ['edge', 'corner']
    filtered_elements = []
    for element in atomic_elements:
        if element[1] in location_types:
            filtered_elements.append(('location', ''))
        elif element[1] not in unknown_types:
            filtered_elements.append(element)

    return filtered_elements

def correct_relations(grammar):
    filtered_grammar = []
    entity_properties = {'color', 'type'}
    for index, element in enumerate(grammar[:-1]):
        if element == 'relation':
            if grammar[index+1] in entity_properties:
                filtered_grammar.append(element)
        else:
            filtered_grammar.append(element)
    if grammar[-1] != 'relation':
        filtered_grammar.append(grammar[-1])

    return filtered_grammar

def change_rcl_to_spanish(rcl):
    new_grammar = []
    grammar_string = ' '.join(rcl)
    grammar_string = re.sub('color color type', 'type color color', grammar_string)
    grammar_string = re.sub('color type', 'type color', grammar_string)
    new_grammar = grammar_string.split(' ')
    return new_grammar



leaves = {}

for scene in range(1, 1001):

    rcl_trees = {}
    sentences = read_sentences(scene)
    scene_dict = {}
    for sentence_id in sentences:
        logging.info('\n')
        logging.info(sentences[sentence_id]['text'])
        # logging.info(sentences[sentence_id]['RCL'])
        rcl = sentences[sentence_id]['RCL']

        logging.info(rcl)
        logging.info('')
        atomic_elements = find_atomic_elements(rcl)
        logging.info(atomic_elements)
        logging.info('')
        atomic_elements = correct_entity_types(atomic_elements)

        valid_elements = {'action', 'type', 'color', 'relation', 'location'}
        grammar = [element[0] for element in atomic_elements if element[0] in valid_elements]

        grammar = correct_relations(grammar)
        grammar = change_rcl_to_spanish(grammar)

        logging.info(grammar)
        scene_dict[sentence_id] = grammar


        pickle.dump(scene_dict, open(DATA_FOLDER + "rcl/" + str(scene) + '_rcl_grammar.p', 'wb'))

        """
        atomic = find_atomic_elements(rcl)
        # print(atomic)
        for a in atomic:
            if not a[0] in leaves:
                leaves[a[0]] = set()
            leaves[a[0]].add(a[1])

        # t = trees_to_dict(rcl)
        # print(t['event']['entity'])
        # rcl_trees[sentence_id] = t

    # pickle.dump(rcl_trees, open(DATA_FOLDER + "scenes/"+ str(scene) + '_rcl.p', 'wb'))
for key in leaves:
    print(key, leaves[key])
"""


