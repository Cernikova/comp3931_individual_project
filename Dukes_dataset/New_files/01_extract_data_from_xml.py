import numpy as np
from data_extensions import *



R = Extension_Robot()

save = 0
save = 'save'		#saving image

for scene in range(1,1001):
#for scene in scenes:
    print("*******************Scene ", scene, " *****************************")

    R.scene = scene
    R._initilize_values()
    R._fix_sentences()
    R._change_shapes_and_colors()
    R._duplicate_objects()
    R._initialize_scene()                 # place the robot and objects in the initial scene position without saving or motion
    R._print_sentences()                  # print the sentences on terminal and remove the SPAM sentence
    R._save_motion()                      # save the motion into a text file
    R._clear_scene()                      # remove the objects from the scene once it's done

    print("///////////////////////////////////////////////////////////////////////////\n\n\n")


print(len(R.all_words))

print("Problem:  ", R.problem)

