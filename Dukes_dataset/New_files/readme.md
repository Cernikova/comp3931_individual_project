# Running Instructions

1. Download the dataset from https://gitlab.com/Cernikova/dukes-dataset
2. Iside the dataset folder i.e. on the same level with the folder
   *scenes* and *rcl*, create folders called *visual-features*, *learning*, *groundings* and *grammar*
3. Go to constants.py and change the DATA constant so that it points to the parent directory of *scenes*
4. run the scripts numbered 02 to 06


