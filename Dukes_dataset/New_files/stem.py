from pickle_functions import *
from stemmer import stem
import itertools

def reverse_stem_ditc(stem_dict):
    reversed_dict = {}
    for suffix in stem_dict:
        for word_root in stem_dict[suffix]:
            if word_root not in reversed_dict:
                reversed_dict[word_root] = set()
            reversed_dict[word_root].add(suffix)
    return reversed_dict


suffix_dict = {}

for scene in range(1, 1001):
    n_grams = read_n_grams(scene)
    single_words = [n_gram for n_gram in n_grams if ' ' not in n_gram]
    word_combinations = itertools.combinations(single_words, 2)
    for comb in word_combinations:
        if '' in comb:
            continue
        word_root, suffixes = stem(comb[0], comb[1])
        if word_root:
            for suffix in suffixes:
                if not suffix in suffix_dict:
                    suffix_dict[suffix] = set()
                suffix_dict[suffix].add(word_root)

valid_suffixes = {}
for suffix in sorted(suffix_dict, key=len):
    if len(suffix_dict[suffix]) > 2:
        print(suffix, "----->", suffix_dict[suffix])
        valid_suffixes[suffix] = suffix_dict[suffix]

print()
word_roots = reverse_stem_ditc(valid_suffixes)
valid_roots = {}
for word_root in sorted(word_roots, key=len):
    if len(word_roots[word_root])>1:
        print(word_root, "----->", word_roots[word_root])
        valid_roots[word_root] = word_roots[word_root]




