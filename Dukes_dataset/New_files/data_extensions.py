from copy import deepcopy
import numpy as np
import logging
from constants import *
from xml_functions import *
from language_translation import translate_sentence
import os
from random import randint
import pickle


class Extension_Robot():
    def __init__(self):
        self.total_num_objects = 0
        self._initilize_values()
        self.all_sentences_count = 1
        self.Data = read_data()
        self.all_words = []
        self.scenes_with_obj_in_the_air = SCENES_WITH_OBJECT_IN_THE_AIR
        self.layouts_with_obj_in_the_air = []
        self.surrounding_squares_are_full = FULL_SCENES
        self.problem = []
        for s in self.Data['scenes']:
            self.Data['scenes'][s]['I_move2'] = -1
            self.Data['scenes'][s]['F_move2'] = -1
        logging.basicConfig(level=logging.INFO)

    def _initilize_values(self):
        self.chess_shift_x = 8
        self.chess_shift_y = 6
        self.len_arm1 = 8
        self.len_arm2 = 6
        self.len_gripper = 2
        self.len_base = 2
        self.l1 = 0
        self.l2 = self.len_arm1
        self.l3 = self.len_arm2 + self.len_gripper
        self.a0 = 0
        self.a1 = 0
        self.a2 = 0
        self.step = 8
        self.frame_number = 0
        self.object = {}
        self.object_shape = {}
        self.words = []
        self.positions = {}
        self.image_dir = DATA_FOLDER + 'scenes/'
        if not os.path.isdir(self.image_dir):
            print('please change the directory in extract_data.py')

    def _fix_sentences(self):
        if self.scene not in self.Data['commands']:
            self.Data['commands'][self.scene] = {}
        S = self.Data['commands'][self.scene]
        for i in S:
            S[i] = S[i].replace("    ", " ")
            S[i] = S[i].replace("   ", " ")
            S[i] = S[i].replace("  ", " ")
            S[i] = S[i].replace("  ", " ")
            S[i] = S[i].replace("  ", " ")
            S[i] = S[i].replace("  ", " ")
            S[i] = S[i].replace(".", "")
            S[i] = S[i].replace(",", "")
            S[i] = S[i].replace("'", "")
            S[i] = S[i].replace("-", " ")
            S[i] = S[i].replace("/", " ")
            S[i] = S[i].replace("!", "")
            S[i] = S[i].replace("(", "")
            S[i] = S[i].replace(")", "")
            S[i] = S[i].replace("?", "")
            S[i] = S[i].replace(" botton ", " bottom ")
            S[i] = S[i].replace(" o nthe ", " on the ")
            S[i] = S[i].replace(" highthest ", " highest ")
            S[i] = S[i].replace(" taht ", " that ")
            S[i] = S[i].replace(" yelllow ", " yellow ")
            S[i] = S[i].replace(" bluee ", " blue ")
            S[i] = S[i].replace(" paced ", " placed ")
            S[i] = S[i].replace(" edhe ", " edge ")
            S[i] = S[i].replace(" gree ", " green ")
            S[i] = S[i].replace(" ed ", " red ")
            S[i] = S[i].replace(" pf ", " of ")
            S[i] = S[i].replace(" tow ", " top ")
            S[i] = S[i].replace(" te ", " the ")
            S[i] = S[i].replace(" if ", " of ")
            S[i] = S[i].replace(" l2 ", " 2 ")
            S[i] = S[i].replace(" re ", " red ")
            S[i] = S[i].replace(" rd ", " red ")
            S[i] = S[i].replace(" op ", " top ")
            S[i] = S[i].replace(" closet ", " closest ")
            S[i] = S[i].replace("pickup ", "pick up ")
            S[i] = S[i].replace(" dearest ", " nearest ")
            S[i] = S[i].replace(" gyellow ", " yellow ")
            S[i] = S[i].replace(" uo ", " up ")
            S[i] = S[i].replace(" un ", " up ")
            S[i] = S[i].replace(" twp ", " two ")
            S[i] = S[i].replace(" blok ", " block ")
            S[i] = S[i].replace(" o ", " on ")
            S[i] = S[i].replace(" thee ", " the ")
            S[i] = S[i].replace(" sian ", " cyan ")
            S[i] = S[i].replace(" he ", " the ")
            S[i] = S[i].replace(" an ", " and ")
            S[i] = S[i].replace(" atop ", " top ")
            S[i] = S[i].replace(" i ton ", " it on ")
            S[i] = S[i].replace(" hte ", " the ")
            S[i] = S[i].replace(" pryamid ", " pyramid ")

            A = S[i].split(' ')
            while '' in A:         A.remove('')
            s = ' '.join(A)
            S[i] = s.lower()
            self.Data['commands_id'][i]['text'] = s.lower()

        self.Data['commands'][self.scene] = S

    """
    Check availability of a given square.
    """

    def _is_available(self, square_x, square_y, layout_index):
        if square_x > 7 or square_x < 0:
            return False
        if square_y > 7 or square_y < 0:
            return False
        for obj in self.Data['layouts'][layout_index]:
            if self.Data['layouts'][layout_index][obj]['position'][0] == square_x \
                    and self.Data['layouts'][layout_index][obj]['position'][1] == square_y \
                    and self.Data['layouts'][layout_index][obj]['position'][2] == 0:
                return False
        return True

    def _add_duplicate_object_to_layout(self, obj_index, obj_x, obj_y, duplicate_index, layout_index,
                                        different_source_layout=False, source_layout_index=-1):

        if different_source_layout:
            self.Data['layouts'][layout_index][duplicate_index] = deepcopy(
                self.Data['layouts'][source_layout_index][obj_index])
        else:
            self.Data['layouts'][layout_index][duplicate_index] = deepcopy(
                self.Data['layouts'][layout_index][obj_index])

        # If possible, place the duplicate object on the right.
        if self._is_available(obj_x + 1, obj_y, layout_index):
            self.Data['layouts'][layout_index][duplicate_index]['position'][0] = obj_x + 1
            return 1
        # Or left.
        if self._is_available(obj_x - 1, obj_y, layout_index):
            self.Data['layouts'][layout_index][duplicate_index]['position'][0] = obj_x - 1
            return 1
        # Or up.
        if self._is_available(obj_x, obj_y + 1, layout_index):
            self.Data['layouts'][layout_index][duplicate_index]['position'][1] = obj_y + 1
            return 1
        # Or down.
        if self._is_available(obj_x, obj_y - 1, layout_index):
            self.Data['layouts'][layout_index][duplicate_index]['position'][1] = obj_y - 1
            return 1
        return 0

    def _add_duplicate_object_on_top(self, obj_index, obj_z, duplicate_index, layout_index):

        self.Data['layouts'][layout_index][duplicate_index] = deepcopy(self.Data['layouts'][layout_index][obj_index])

        self.Data['layouts'][layout_index][duplicate_index]['position'][2] = obj_z + 1

    def _change(self, sentence_id, words, key, sentence_array):
        # Loop through the list of words we want to change for something else.
        for i, word in enumerate(words):
            # Find the position(s) of the word within the sentence.
            indices = [j for j, x in enumerate(sentence_array) if x == word]
            # Change the words in the sentence for the corresponding variation.
            for m in indices:
                sentence_array[m] = key[i]
        self.Data['commands'][self.scene][sentence_id] = ' '.join(sentence_array)

    def _pluralise_moving_entity(self, sentence_id, change_from, change_to, sentence_array, aggr=False, aggr_name=None):
        first_occurrence_found = False

        # Turning this feature off for now
        aggr = False
        for sentence_position, word in enumerate(sentence_array):
            if first_occurrence_found:
                break
            for list_index, to_be_changed in enumerate(change_from):
                if word == to_be_changed:
                    sentence_array[sentence_position] = change_to[list_index]

                    # add an aggregate name to the sentence
                    if aggr:
                        # put it in front of the shape (and color) of the objects
                        if sentence_array[sentence_position - 1] in colors:
                            sentence_position -= 1
                        sentence_array.insert(sentence_position, 'of')
                        sentence_array.insert(sentence_position, aggr_name)
                    first_occurrence_found = True
                    break
        self.Data['commands'][self.scene][sentence_id] = ' '.join(sentence_array)

    def _change_first_occurrence(self, sentence_id, change_from, change_to, sentence_array, aggr=False, aggr_name=None):
        first_occurrence_found = False

        # Turning this feature off for now
        aggr = False
        for sentence_position, word in enumerate(sentence_array):
            if first_occurrence_found:
                break
            for list_index, to_be_changed in enumerate(change_from):
                if word == to_be_changed:
                    sentence_array[sentence_position] = change_to[list_index]

                    # add an aggregate name to the sentence
                    if aggr:
                        # put it in front of the shape (and color) of the objects
                        if sentence_array[sentence_position - 1] in colors:
                            sentence_position -= 1
                        sentence_array.insert(sentence_position, 'of')
                        sentence_array.insert(sentence_position, aggr_name)
                    first_occurrence_found = True
                    break
        self.Data['commands'][self.scene][sentence_id] = ' '.join(sentence_array)

    """
    Duplicate objects in the scenes.
    """
    def _duplicate_objects(self):

        def _find_moving_object_in_final_layout(layout_id):
            for obj_id in self.Data['layouts'][layout_id]:
                if obj_id == self.Data['scenes'][self.scene]['F_move']:
                    return obj_id
            return -1

        all_object_singulars = []
        for arr in [prism_singulars, cube_singulars, sphere_singulars, cylinder_singulars]:
            all_object_singulars.extend(arr)

        all_object_plurals = []
        for arr in [prism_plurals, cube_plurals, sphere_plurals, cylinder_plurals]:
            all_object_plurals.extend(arr)

        duplicate_in_row = 0
        duplicate_on_top = 0
        if self.scene in self.scenes_with_obj_in_the_air or self.scene in self.surrounding_squares_are_full:
            duplicate_on_top = randint(0, 1)
        else:
            duplicate_in_row = randint(0, 1)

        if duplicate_in_row or duplicate_on_top:
            # change scenes
            # Each scene is defined by its initial and final layout.
            I = self.Data['scenes'][self.scene]['initial']
            F = self.Data['scenes'][self.scene]['final']

            # Change the scenes's initial and final layout pointers to new layouts that will be created.
            self.Data['scenes'][self.scene]['initial'] = 1000 + I
            self.Data['scenes'][self.scene]['final'] = 1000 + F

            success = 1
            mov_obj = -1
            bottom_objs_final = []
            bottom_objs_init = []
            mov_obj_init_x = -1
            mov_obj_init_y = -1
            mov_obj_final_x = -1
            mov_obj_final_y = -1
            mov_obj_final_z = -1
            pair_id = -1
            object_in_the_air = False

            # change layouts
            # Copy each pair of initial anf final layout I, F to a new pair of layouts 1000+I, 1000+F.
            self.Data['layouts'][1000 + I] = {}
            self.Data['layouts'][1000 + F] = {}
            for obj in self.Data['layouts'][I]:
                self.Data['layouts'][1000 + I][obj] = dict(self.Data['layouts'][I][obj])
            for obj in self.Data['layouts'][F]:
                self.Data['layouts'][1000 + F][obj] = dict(self.Data['layouts'][F][obj])

            # add gripper positions in the new layouts to the list of all gripper positions
            self.Data['gripper'][1000 + I] = self.Data['gripper'][I]
            self.Data['gripper'][1000 + F] = self.Data['gripper'][F]

            n_objects = len(self.Data['layouts'][1000 + I])
            # Get the initial and final coordinates of the moving object.
            for obj in self.Data['layouts'][1000 + I]:
                if obj == self.Data['scenes'][self.scene]['I_move']:
                    pair_id = _find_moving_object_in_final_layout(1000 + F)
                    if pair_id == -1:
                        logging.error("Moved object not found in final layout!!!!")
                        continue
                    logging.info("Scene %d, moving object %s %s, id %d." % (self.scene,
                                                                            self.Data['layouts'][1000 + I][obj][
                                                                                'F_HSV'],
                                                                            self.Data['layouts'][1000 + I][obj][
                                                                                'F_SHAPE'], obj))

                    logging.info("x coordinates: %d, %d;  y coordinates: %d, %d." %
                                 (self.Data['layouts'][1000 + I][obj]['position'][0],
                                  self.Data['layouts'][1000 + F][pair_id]['position'][0],
                                  self.Data['layouts'][1000 + I][obj]['position'][1],
                                  self.Data['layouts'][1000 + F][pair_id]['position'][1])
                                 )
                    mov_obj = obj
                    mov_obj_init_x = self.Data['layouts'][1000 + I][obj]['position'][0]
                    mov_obj_init_y = self.Data['layouts'][1000 + I][obj]['position'][1]
                    mov_obj_init_z = self.Data['layouts'][1000 + I][obj]['position'][2]
                    mov_obj_final_x = self.Data['layouts'][1000 + F][pair_id]['position'][0]
                    mov_obj_final_y = self.Data['layouts'][1000 + F][pair_id]['position'][1]
                    mov_obj_final_z = self.Data['layouts'][1000 + F][pair_id]['position'][2]

                    if duplicate_in_row:
                        # If the object is not initially on the ground
                        if mov_obj_init_z > 0:
                            # Find bottom objects
                            bottom_objs_init = self._find_bottom_objects(mov_obj, self.Data['layouts'][1000 + I])
                            # The object is in the air
                            if mov_obj_init_z > len(bottom_objs_init):
                                object_in_the_air = True
                                logging.info("Objects in the air in scene %d layout %d" % (self.scene, I))

                        # If the object is not on the ground after it's been moved.
                        if mov_obj_final_z > 0:
                            bottom_objs_final = self._find_bottom_objects(pair_id, self.Data['layouts'][1000 + F])
                            logging.info("Bottom objects: %s" % bottom_objs_final)

            if mov_obj == -1:
                logging.error("Moving object not found in layout %d scene %d." % (self.scene, I))
            else:
                # Duplicate the objects:

                if duplicate_in_row:
                    # The moving object:
                    self.Data['scenes'][self.scene]['I_move2'] = n_objects
                    if not self._add_duplicate_object_to_layout(mov_obj, mov_obj_init_x, mov_obj_init_y, n_objects,
                                                                1000 + I):
                        success = 0
                        logging.warning("Couldn't duplicate the object.")
                    self.Data['scenes'][self.scene]['F_move2'] = n_objects
                    if not self._add_duplicate_object_to_layout(pair_id, mov_obj_final_x, mov_obj_final_y, n_objects,
                                                                1000 + F):
                        success = 0
                        logging.warning("Couldn't duplicate the object.")

                    logging.info("---------------------Moving object, initial layout---------------------")
                    logging.info("Original: %s" % self.Data['layouts'][1000 + I][mov_obj])
                    logging.info("Duplicate: %s" % self.Data['layouts'][1000 + I][n_objects])
                    logging.info("-----------------------------------------------------------------------")
                    logging.info("---------------------Moving object, final layout-----------------------")
                    logging.info("Original: %s" % self.Data['layouts'][1000 + F][pair_id])
                    logging.info("Duplicate: %s" % self.Data['layouts'][1000 + F][n_objects])
                    logging.info("-----------------------------------------------------------------------")

                    # Underlying objects, final scene:
                    bottom_objs_final.sort(reverse=True)
                    for count, bottom_obj in enumerate(bottom_objs_final, 1):
                        if self._add_duplicate_object_to_layout(bottom_obj, mov_obj_final_x, mov_obj_final_y,
                                                                n_objects + count,
                                                                1000 + I, different_source_layout=True,
                                                                source_layout_index=1000 + F):
                            logging.info("-----------------Final bottom object, initial layout-------------------")
                            logging.info("Original: %s" % self.Data['layouts'][1000 + F][bottom_obj])
                            logging.info("Duplicate: %s" % self.Data['layouts'][1000 + I][n_objects + count])
                            logging.info("-----------------------------------------------------------------------")
                        else:
                            logging.warning("Couldn't duplicate underlying object.")
                            success = 0

                        if self._add_duplicate_object_to_layout(bottom_obj, mov_obj_final_x, mov_obj_final_y,
                                                                n_objects + count, 1000 + F):
                            logging.info("-----------------Final bottom object, final layout---------------------")
                            logging.info("Original: %s" % self.Data['layouts'][1000 + F][bottom_obj])
                            logging.info("Duplicate: %s" % self.Data['layouts'][1000 + F][n_objects + count])
                            logging.info("-----------------------------------------------------------------------")
                        else:
                            logging.warning("Couldn't duplicate underlying object.")
                            success = 0

                    # Underlying objects, initial scene
                    bottom_objs_init.sort(reverse=True)
                    for count, bottom_obj in enumerate(bottom_objs_init, len(self.Data['layouts'][1000 + I])):
                        if self._add_duplicate_object_to_layout(bottom_obj, mov_obj_init_x, mov_obj_init_y, count,
                                                                1000 + I):
                            logging.info("--------------Initial bottom object, initial layout--------------------")
                            logging.info("Original: %s" % self.Data['layouts'][1000 + F][bottom_obj])
                            logging.info("Duplicate: %s" % self.Data['layouts'][1000 + I][count])
                            logging.info("-----------------------------------------------------------------------")
                        else:
                            logging.warning("Couldn't duplicate underlying object.")
                            success = 0

                        if self._add_duplicate_object_to_layout(bottom_obj, mov_obj_init_x, mov_obj_init_y, count,
                                                                1000 + F,
                                                                different_source_layout=True,
                                                                source_layout_index=1000 + I):
                            logging.info("----------------Initial bottom object, final layout--------------------")
                            logging.info("Original: %s" % self.Data['layouts'][1000 + F][bottom_obj])
                            logging.info("Duplicate: %s" % self.Data['layouts'][1000 + F][count])
                            logging.info("-----------------------------------------------------------------------")
                        else:
                            logging.warning("Couldn't duplicate underlying object.")
                            success = 0

                    if not success:
                        self.problem.append(self.scene)

                    # change commands
                    if success:
                        for sentence_id in self.Data['commands'][self.scene]:
                            s = self.Data['commands'][self.scene][sentence_id].split(' ')

                            # Change singular forms to plural forms in the sentences.
                            self._change_first_occurrence(sentence_id, all_object_singulars, all_object_plurals, s,
                                                          True, 'row')
                            self._change(sentence_id, all_object_singulars, all_object_plurals, s)
                            self._change(sentence_id, other_singulars, other_plurals, s)

                elif duplicate_on_top:
                    # The moving object:
                    self.Data['scenes'][self.scene]['I_move2'] = n_objects
                    self._add_duplicate_object_on_top(mov_obj, mov_obj_init_z, n_objects, 1000 + I)

                    self.Data['scenes'][self.scene]['F_move2'] = n_objects
                    self._add_duplicate_object_on_top(pair_id, mov_obj_final_z, n_objects, 1000 + F)

                    logging.info("---------------------Moving object, initial layout---------------------")
                    logging.info("Original: %s" % self.Data['layouts'][1000 + I][mov_obj])
                    logging.info("Duplicate: %s" % self.Data['layouts'][1000 + I][n_objects])
                    logging.info("-----------------------------------------------------------------------")
                    logging.info("---------------------Moving object, final layout-----------------------")
                    logging.info("Original: %s" % self.Data['layouts'][1000 + F][pair_id])
                    logging.info("Duplicate: %s" % self.Data['layouts'][1000 + F][n_objects])
                    logging.info("-----------------------------------------------------------------------")

                    if not success:
                        self.problem.append(self.scene)

                    # change commands
                    for sentence_id in self.Data['commands'][self.scene]:
                        s = self.Data['commands'][self.scene][sentence_id].split(' ')

                        # Change singular forms to plural forms in the sentences.
                        self._change_first_occurrence(sentence_id, all_object_singulars, all_object_plurals, s, True,
                                                      'column')
                        self._change(sentence_id, other_singulars, other_plurals, s)

    def fix_rcl_trees(self, rcl_tree):
        pass


    """
    Create scenes with object/colour variations.
    """
    def _change_shapes_and_colors(self):

        def _change(words, key):
            # Loop through the list of words we want to change for something else.
            for i, word in enumerate(words):
                # Find the position(s) of the word within the sentence.
                indices = [j for j, x in enumerate(s) if x == word]
                # Change the word for the corresponding variation.
                for m in indices:
                    s[m] = key[i]
            self.Data['commands'][self.scene][sentence] = ' '.join(s)

        change_prism = ['pyramid', 'prism', 'tetrahedron', 'triangle']
        change_prism_to = ['ball', 'sphere', 'orb', 'orb']
        change_prisms = ['pyramids', 'prisms', 'tetrahedrons', 'triangles']
        change_prisms_to = ['balls', 'spheres', 'orbs', 'orbs']
        change_box = ['block', 'cube', 'box', 'slab', 'parallelipiped', 'parallelepiped', 'brick', 'square']
        change_box_to = ['cylinder', 'can', 'drum', 'drum', 'can', 'can', 'can', 'can']
        change_boxes = ['cubes', 'boxes', 'blocks', 'slabs', 'parallelipipeds', 'bricks', 'squares']
        change_boxes_to = ['cylinders', 'cans', 'drums', 'drums', 'cans', 'cans', 'cans']

        a1 = randint(0, 1)
        a2 = randint(0, 1)
        a3 = randint(0, 1)

        c = 'nothing'
        d = 'nothing'
        e = 'nothing'

        if a1:            c = 'black'
        if a2:            d = 'sphere'  # orb, ball
        if a3:            e = 'cylinder'  # can,

        # if self.scene % 2 == 1:
        # change commands
        for sentence in self.Data['commands'][self.scene]:
            s = self.Data['commands'][self.scene][sentence].split(' ')
            # Change prisms to spheres.
            if d == 'sphere':
                _change(change_prism, change_prism_to)
                _change(change_prisms, change_prisms_to)
            # Change cubes to cylinders.
            if e == 'cylinder':
                _change(change_box, change_box_to)
                _change(change_boxes, change_boxes_to)
            # Change red to black.
            if c != 'nothing':
                _change(['red', 'maroon'], ['black', 'black'])

        # change scenes
        # Each scene is defined by its initial and final layout.
        I = self.Data['scenes'][self.scene]['initial']
        F = self.Data['scenes'][self.scene]['final']

        # Change the scenes's initial and final layout pointers to new layouts that will be created.
        self.Data['scenes'][self.scene]['initial'] = 1000 + I
        self.Data['scenes'][self.scene]['final'] = 1000 + F

        # change layouts
        # Copy each pair of initial anf final layout I, F to a new pair of layouts 1000+I, 1000+F.
        self.Data['layouts'][1000 + I] = {}
        self.Data['layouts'][1000 + F] = {}
        for obj in self.Data['layouts'][I]:
            self.Data['layouts'][1000 + I][obj] = dict(self.Data['layouts'][I][obj])
        for obj in self.Data['layouts'][F]:
            self.Data['layouts'][1000 + F][obj] = dict(self.Data['layouts'][F][obj])

        # Change cubes to cylinders in the new initial layout.
        for obj in self.Data['layouts'][1000 + I]:
            if e == 'cylinder':
                if self.Data['layouts'][1000 + I][obj]['F_SHAPE'] == 'cube':
                    self.Data['layouts'][1000 + I][obj]['F_SHAPE'] = 'cylinder'
            # Change prisms to spheres in the new initial layout.
            if d == 'sphere':
                if self.Data['layouts'][1000 + I][obj]['F_SHAPE'] == 'prism':
                    self.Data['layouts'][1000 + I][obj]['F_SHAPE'] = 'sphere'
        # Change cubes to cylinders in the new final layout.
        for obj in self.Data['layouts'][1000 + F]:
            if e == 'cylinder':
                if self.Data['layouts'][1000 + F][obj]['F_SHAPE'] == 'cube':
                    self.Data['layouts'][1000 + F][obj]['F_SHAPE'] = 'cylinder'
            # Change prisms to spheres in the new final layout.
            if d == 'sphere':
                if self.Data['layouts'][1000 + F][obj]['F_SHAPE'] == 'prism':
                    self.Data['layouts'][1000 + F][obj]['F_SHAPE'] = 'sphere'

        # Change red to black in the new initial and final layout.
        if c != 'nothing':
            for obj in self.Data['layouts'][1000 + I]:
                if self.Data['layouts'][1000 + I][obj]['F_HSV'] == 'red':
                    self.Data['layouts'][1000 + I][obj]['F_HSV'] = c

            for obj in self.Data['layouts'][1000 + F]:
                if self.Data['layouts'][1000 + F][obj]['F_HSV'] == 'red':
                    self.Data['layouts'][1000 + F][obj]['F_HSV'] = c

        # change gripper
        self.Data['gripper'][1000 + I] = self.Data['gripper'][I]
        self.Data['gripper'][1000 + F] = self.Data['gripper'][F]

    def _print_sentences(self):
        scene = self.scene
        self.sentences = {}
        to_be_poped = []
        for count, i in enumerate(self.Data['commands'][scene]):
            if i not in self.Data['comments']:
                print(count, '-', self.Data['commands'][scene][i])
                self.all_sentences_count += 1
                self.sentences[count] = ['GOOD', self.Data['commands'][scene][i]]
                for word in self.Data['commands'][scene][i].split(' '):
                    if word not in self.all_words:
                        self.all_words.append(word)
            else:
                to_be_poped.append(i)
        for i in to_be_poped:
            self.Data['commands'][scene].pop(i)
            self.Data['commands_id'].pop(i)
            self.Data['RCL'].pop(i)
        print('--------------------------')

    def translate_to_spanish(self):
        for sentence_id in self.Data['commands'][self.scene]:
            sentence_text = self.Data['commands'][self.scene][sentence_id]
            self.Data['commands'][self.scene][sentence_id] = translate_sentence(sentence_text, 'es')

    def _initialize_scene(self):
        self._add_objects_to_scene()
        self._initialize_robot()

    def _is_top_object(self, obj, layout):
        x = layout[obj]['position'][0]
        y = layout[obj]['position'][1]
        z = layout[obj]['position'][2]
        top_object = 1
        for obj2 in layout:
            if obj2 != obj:
                x2 = layout[obj2]['position'][0]
                y2 = layout[obj2]['position'][1]
                z2 = layout[obj2]['position'][2]
                if x2 == x and y2 == y and z2 > z:
                    top_object = 0
        return top_object

    def _find_bottom_objects(self, obj, layout):
        x = layout[obj]['position'][0]
        y = layout[obj]['position'][1]
        z = layout[obj]['position'][2]
        bottom_objects = []
        for obj2 in layout:
            if obj2 != obj:
                x2 = layout[obj2]['position'][0]
                y2 = layout[obj2]['position'][1]
                z2 = layout[obj2]['position'][2]
                if x2 == x and y2 == y and z2 < z:
                    bottom_objects.append(obj2)
        return bottom_objects

    def _get_towers(self, layout):
        groups = {}
        height = {}
        colours = {}
        shapes = {}
        towers = {}
        for obj in layout:
            if obj != 'gripper':
                x = layout[obj]['position'][0]
                y = layout[obj]['position'][1]
                z = layout[obj]['position'][2]
                rgb = layout[obj]['F_HSV']
                shape = layout[obj]['F_SHAPE']
                if shape in ['cube', 'cylinder']:
                    if (x, y) not in groups:
                        groups[(x, y)] = 1
                        height[(x, y)] = z
                        colours[(x, y)] = rgb
                        shapes[(x, y)] = 'tower'
                    else:
                        groups[(x, y)] += 1
                        if z > height[(x, y)]:
                            height[(x, y)] = z
                        if rgb not in colours[(x, y)]:
                            colours[(x, y)] += '-' + rgb
                        if shape not in shapes[(x, y)]:
                            shapes[(x, y)] += '-' + shape

        for i in groups:
            if groups[i] > 1:
                key = np.max(list(self.positions.keys())) + 1
                self.positions[key] = {}
                self.positions[key]['x'] = [i[0], i[0]]
                self.positions[key]['y'] = [i[1], i[1]]
                self.positions[key]['z'] = [height[i], height[i]]
                self.positions[key]['F_HSV'] = colours[i]
                self.positions[key]['F_SHAPE'] = 'tower'
                self.positions[key]['moving'] = 0

    def _add_objects_to_scene(self):
        self.frame_number = 0
        l1 = self.Data['layouts'][self.Data['scenes'][self.scene]['initial']]  # initial layout
        print(l1)
        for obj in l1:
            self.total_num_objects += 1
            x = l1[obj]['position'][0]
            y = l1[obj]['position'][1]
            z = l1[obj]['position'][2]

            # initializing the position vector to be saved later
            self.positions[obj] = {}
            self.positions[obj]['x'] = [int(x)]
            self.positions[obj]['y'] = [int(y)]
            self.positions[obj]['z'] = [int(z)]
            self.positions[obj]['F_HSV'] = l1[obj]['F_HSV']
            self.positions[obj]['F_SHAPE'] = l1[obj]['F_SHAPE']
            if obj != self.Data['scenes'][self.scene]['I_move'] and obj != self.Data['scenes'][self.scene]['I_move2']:
                self.positions[obj]['x'] = [int(x), int(x)]
                self.positions[obj]['y'] = [int(y), int(y)]
                self.positions[obj]['z'] = [int(z), int(z)]
                self.positions[obj]['moving'] = 0
            else:
                self.positions[obj]['moving'] = 1

        self._get_towers(l1)

        I = self.Data['scenes'][self.scene]['I_move']
        I2 = self.Data['scenes'][self.scene]['I_move2']

        l1 = self.Data['layouts'][self.Data['scenes'][self.scene]['final']]  # initial layput
        for obj in l1:
            if obj == self.Data['scenes'][self.scene]['F_move']:
                x = l1[obj]['position'][0]
                y = l1[obj]['position'][1]
                z = l1[obj]['position'][2]
                # print '>>;',x,y,z
                self.positions[I]['x'].append(int(x))
                self.positions[I]['y'].append(int(y))
                self.positions[I]['z'].append(int(z))

            if obj == self.Data['scenes'][self.scene]['F_move2']:
                x = l1[obj]['position'][0]
                y = l1[obj]['position'][1]
                z = l1[obj]['position'][2]
                # print '>>;',x,y,z
                self.positions[I2]['x'].append(int(x))
                self.positions[I2]['y'].append(int(y))
                self.positions[I2]['z'].append(int(z))

    def _initialize_robot(self):
        initial_position = self.Data['gripper'][self.Data['scenes'][self.scene]['initial']]
        final_position = self.Data['gripper'][self.Data['scenes'][self.scene]['final']]
        self.positions['gripper'] = {}
        self.positions['gripper']['x'] = [int(initial_position[0]), int(final_position[0])]
        self.positions['gripper']['y'] = [int(initial_position[1]), int(final_position[1])]
        self.positions['gripper']['z'] = [int(initial_position[2]), int(final_position[2])]

    def _update_scene_number(self):
        self.label.text = 'Scene number : ' + str(self.scene)

    def _save_motion(self):
        F = open(self.image_dir + str(self.scene) + '_sentences' + '.txt', 'w')
        for i in self.sentences:
            F.write(self.sentences[i][1] + '\n')
        F.close()

        F = open(self.image_dir + str(self.scene) + '_layout' + '.txt', 'w')
        for key in self.positions:
            F.write('object:' + str(key) + '\n')
            F.write('x:')
            x = self.positions[key]['x']
            F.write(str(x[0]) + ',' + str(x[1]))
            F.write("\n")
            F.write('y:')
            y = self.positions[key]['y']
            F.write(str(y[0]) + ',' + str(y[1]))
            F.write("\n")
            F.write('z:')
            z = self.positions[key]['z']
            F.write(str(z[0]) + ',' + str(z[1]))
            F.write("\n")
            if key != 'gripper':
                c = self.positions[key]['F_HSV']
                s = self.positions[key]['F_SHAPE']
                F.write('F_RGB:' + c)
                F.write("\n")
                F.write('F_SHAPE:' + s)
                F.write("\n")
        F.close()
        pickle.dump(self.positions, open(self.image_dir + str(self.scene) + '_layout.p', 'wb'))

        sentence = {}
        for id in self.Data['commands'][self.scene]:
            sentence[id] = {}
            sentence[id]['text'] = self.Data['commands'][self.scene][id]
            sentence[id]['RCL'] = self.Data['RCL'][id]
        pickle.dump(sentence, open(self.image_dir + str(self.scene) + '_sentences.p', 'wb'))

    def _clear_scene(self):
        keys = self.object.keys()
        for i in keys:
            self.object[i].visible = False
            self.object.pop(i)
            self.object_shape.pop(i)
