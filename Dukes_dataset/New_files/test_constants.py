GROUND_TRUTH_DICT = {
    # colors
    'color_black': ["black"],
    'color_blue': ["blue"],
    'color_cyan': ["cyan", "sky", "sky blue", "turquoise"],
    'color_gray': ["gray", "grey"],
    'color_green': ["green"],
    'color_magenta': ["magenta", "pink", "purple"],
    'color_red': ["red"],
    'color_white': ["white"],
    'color_yellow': ["yellow"],
    # shapes
    'type_cube': ["block", "blocks", "parallelipiped", "cube", "cubes", "brick", "bricks"],
    'type_cylinder': ["can", "cans", "cylinder", "cylinders", "drum", "drums"],
    'type_prism': ["prism", "prisms", "pyramid", "pyramids", "tetrahedron"],
    'type_sphere': ["sphere", "orb", "ball", "spheres", "orbs", "balls"],
    'type_tower': ["tower", "stack", "squares"],
    # actions
    "action_type_1": ["hold", "grab", "lift", "pick", "pick up", "remove", "take", "move", "shift"],
    "action_type_2": ["put", "place", "drop", "down", "move"],
    # locations
    "location_(0.0, 0.0)": ["bottom right", "bottom right corner"],
    "location_(0.0, 7.0)": ["bottom left", "bottom left corner"],
    "location_(3.5, 3.5)": ["center", "center of"],
    "location_(7.0, 7.0)": ["top left", "top left corner", "far left corner"],
    "location_(7.0, 0.0)": ["top right", "top right corner", "far right corner"],
    # relations
    "relation_(0.0, 1.0, 0.0)": ["left of"],
    "relation_(0.0, -1.0, 0.0)": ["right of"],
    "relation_(0.0, 0.0, 1.0)": ["on", "onto", "over", "onto", "over", "top of", "on top of", "from"],
    "relation_(1.0, 0.0, 0.0)": ["in front of"],
    "relation_(-1.0, 0.0, 0.0)": ["behind"]
}

GROUND_TRUTH_DICT_SPANISH = {
    # colors
    'color_black': ["negro", "negra", "negros", "negras"],
    'color_blue': ["azul", "azules"],
    'color_cyan': ["cian", "cielo", "cielo azul", "turquesa"],
    'color_gray': ["gris", "grises"],
    'color_green': ["verde"],
    'color_magenta': ["magenta", "rosa", "púrpura"],
    'color_red': ["rojo", "roja", "rojos"],
    'color_white': ["blanco", "blanca", "blancos", "blancas"],
    'color_yellow': ["amarillo", "amarilla", "amarillos", "amarillas"],
    # shapes
    'type_cube': ["bloque", "bloques", "cubo", "cubos", "paralelepípedo", "ladrillo", "ladrillos"],
    'type_cylinder': ["lata", "latas", "cilindro", "cilindros"],
    'type_prism': ["prisma", "pirámide", "tetraedro"],
    'type_sphere': ["esfera", "orbe", "bola", "esferas", "orbes", "bolas"],
    'type_tower': ["torre"],
    # actions
    "action_type_1": ["recoge", "mueve", "mueva", "toma", "cambia", "sostenga"],
    "action_type_2": ["mueve", "mueva", "pon", "coloca", "suelta", "dejó caer", "dejó"],
    # locations
    "location_(0.0, 0.0)": ["inferior derecha", "esquina inferior derecha"],
    "location_(0.0, 7.0)": ["inferior izquierda", "esquina inferior izquierda"],
    "location_(3.5, 3.5)": ["centro"],
    "location_(7.0, 7.0)": ["superior izquierda", "esquina superior izquierda", "lejana izquierda"],
    "location_(7.0, 0.0)": ["superior derecha", "esquina superior derecha", "lejana derecha"],
    # relations
    "relation_(0.0, 1.0, 0.0)": ["izquierda a"],
    "relation_(0.0, -1.0, 0.0)": ["de derecha", "derecha a"],
    "relation_(0.0, 0.0, 1.0)": ["sobre", "encima", "encima de", "encima del"],
    "relation_(1.0, 0.0, 0.0)": ["delante", "delante de"],
    "relation_(-1.0, 0.0, 0.0)": ["detrás", "detrás de"]
}
