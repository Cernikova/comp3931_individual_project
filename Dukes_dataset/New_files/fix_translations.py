import pickle
from pickle_functions import read_sentences
from constants import *

# List all desired changes in the form: <to be changed>:<change to>
correction_dict = {#' muévela en el cubo ': ' muévela sobre el cubo ',
                   #' muévela en la lata ': ' muévela sobre la lata ',
                    #' está en el cubo ': ' está encima del cubo ',
                    #' está en la lata ': ' está encima de la lata ',
                    # ' colodado en el cilindro ': ' colocado encima del cilindro ',
                    # ' colodada en el cilindro ': ' colocada encima del cilindro ',
                    # ' en el cilindro ': ' sobre el cilindro ',
                    'move ': 'mueve '
                   }

for scene in range(500, 1001):
    print('*********************** ', str(scene), '*****************************')
    sentences = read_sentences(scene)
    text_file = open(DATA_FOLDER + 'scenes/' + str(scene) + '_sentences' + '.txt', 'w')
    for sentence_id in sentences:
        corrected = False
        sentence_text = sentences[sentence_id]['text']
        sentence_text_original = sentence_text
        if sentence_text.strip() != sentence_text:
            sentence_text = sentence_text.strip()
            corrected = True
        for mistake in correction_dict:
            if mistake in sentence_text:
                sentence_text = sentence_text.replace(mistake, correction_dict[mistake])
                corrected = True
        if corrected:
                sentences[sentence_id]['text'] = sentence_text
                print('\n%s \n%s \n' % (sentence_text_original, sentences[sentence_id]['text']))
        text_file.write(sentence_text + '\n')
    pkl_file = DATA_FOLDER + 'scenes/' + str(scene) + '_sentences.p'
    pickle.dump(sentences, open(pkl_file, 'wb'))
    text_file.close()