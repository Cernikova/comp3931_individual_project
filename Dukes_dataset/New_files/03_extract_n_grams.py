import numpy as np
from constants import *
import pickle
from pickle_functions import *


"""
Find all n-grams of length 1<=n<=3 from a given sentence.
"""
def _find_n_grams(sentence):
    # Maximum length of n-grams
    n_word = 3
    w = sentence.split(' ')
    n_grams = []
    for i in range(len(w)):
        for j in range(i+1,np.min([i+1+n_word,len(w)+1])):
            n_grams.append(' '.join(w[i:j]))
    return n_grams

"""
:return all n-grams that do not contain stopwords. 
"""
def _get_n_grams(sentences,stop):
    all_n_grams = []
    for id in sentences:
        n = _find_n_grams(sentences[id]['text'])
        for i in n:
            ok = 1
            for stop_word in stop:
                if stop_word in i.split(' '):
                    ok = 0
                    break
                # if stop_word == i or ' '+stop_word in i or stop_word+' ' in i:
                #     ok = 0
            if ok:
                if i not in all_n_grams:
                    all_n_grams.append(i)
    return all_n_grams

stop = read_stop_words()

for scene in range(1,1001):
    print('extracting feature from scene : ',scene)
    # Save the valid n-grams (without stopwords)
    pkl_file = DATA_FOLDER+'learning/'+str(scene)+'_linguistic_features.p'
    LF = {}
    sentences = read_sentences(scene)
    LF['n_grams'] = _get_n_grams(sentences,stop)
    pickle.dump(LF, open(pkl_file, 'wb'))
    file1 = DATA_FOLDER+'learning/'+str(scene)+'_linguistic_feature.txt'
    F = open(file1, 'w')
    for n in LF['n_grams']:
        F.write(n+'\n')
    F.close()
