from constants import *
import pickle
import itertools
from formats import*
import numpy as np
import logging

logging.basicConfig(level=logging.INFO)


def read_pickle(scene):
    pkl_file = DATA_FOLDER+'scenes/'+str(scene)+'_layout.p'
    data = open(pkl_file, 'rb')
    positions = pickle.load(data)
    return positions

"""
Find out what type of action the robot performed (either grasp and pick up the object, release it or both), 
based on the relationship between the gripper and the object that is being moved.
"""
def _get_actions(moving_object_id, positions):
    x_O = positions[moving_object_id]['x']
    y_O = positions[moving_object_id]['y']
    z_O = positions[moving_object_id]['z']

    x_R = positions['gripper']['x']
    y_R = positions['gripper']['y']
    z_R = positions['gripper']['z']

    """
    case 1: End position of the object and gripper is equal.
    case 2: Start position of the object and gripper is equal.
    case 3: The object has moved but we're not sure.
    """
    if x_O[1]==x_R[1] and y_O[1]==y_R[1] and z_O[1]==z_R[1]:
        return ['action_type_1']
    elif x_O[0]==x_R[0] and y_O[0]==y_R[0] and z_O[0]==z_R[0]:
        return ['action_type_2']
    else: #if x_O[0]!=x_O[1] or y_O[0]!=y_O[1] or z_O[0]!=z_O[1]:
        return ['action_type_1', 'action_type_2']
    # else:
    #     return []


def get_moving_object_dict(moving_objects, positions):
    entities = [get_entity_tree(obj_id, positions) for obj_id in moving_objects]
    identical_entities = {}
    for entity_tree in entities:
        dict_key = (entity_tree['shape'], entity_tree['color'])
        if dict_key not in identical_entities:
            identical_entities[dict_key] = {}
            identical_entities[dict_key]['ids']= set()
            identical_entities[dict_key]['locations'] = set()
        identical_entities[dict_key]['ids'].add(entity_tree['id'])
        identical_entities[dict_key]['locations'].add(entity_tree['location'])

        identical_entities[dict_key]['relations'] = {}
        static_objects = set(positions.keys()) - set(moving_objects).union(['gripper'])
        static_moving = itertools.product(static_objects, moving_objects[:1])
        for pair in static_moving:
            relation_initial = get_initial_relation_to(pair[1], pair[0], positions)
            if validate_relation(relation_initial):
                relation_initial_string = RELATION_FORMAT % relation_initial
                identical_entities[dict_key]['relations'][relation_initial_string] = get_entity_tree(pair[0], positions)

    for entity_key in identical_entities:
        ids = identical_entities[entity_key]['ids']
        if len(ids) > 1:
            identical_entities[entity_key]['aggregate'] = get_aggregate_type(ids, positions)
    return identical_entities


def get_aggregate_type(object_ids, positions):
    all_options = itertools.product(object_ids, object_ids)
    valid_options = [o for o in all_options if o[0] < o[1]]
    all_relations = []
    for o in valid_options:
        all_relations.append(get_initial_relation_to(o[0], o[1], positions))

    axes_dict = {'x': sum(abs(t[0]) for t in all_relations),
                 'y': sum(abs(t[1]) for t in all_relations),
                 'z': sum(abs(t[2]) for t in all_relations)}
    axes = list(filter(lambda k: axes_dict[k] > 0, axes_dict))
    if axes:
        return 'aggregate_' + ','.join(axes)
    return None


def get_event_tree(positions):
    moving_objects = []
    object_coordinates = []
    for obj in positions:
        if obj != 'gripper':
            if positions[obj]['moving']:
                moving_objects.append(obj)

    if moving_objects:
        # We only need the action performed with the first object
        actions = _get_actions(moving_objects[0], positions)

        entities = get_moving_object_dict(moving_objects, positions)


        tree = {'actions': actions,
                'entities': entities,
                'destination': {}
                }
        tree['destination']['absolute'] = get_final_location(moving_objects[0], positions)
        tree['destination']['relative'] = {}
        static_objects = set(positions.keys()) - set(moving_objects).union(['gripper'])
        static_moving = itertools.product(static_objects, moving_objects[:1])
        for pair in static_moving:
            relation_final = get_final_relation_to(pair[1], pair[0], positions)
            if validate_relation(relation_final):
                relation_final_string = RELATION_FORMAT % relation_final
                tree['destination']['relative'][relation_final_string] = get_entity_tree(pair[0], positions)
    else:
        tree = {}
    return tree


def get_entity_tree(object_id, positions):
    tree = {'id':object_id}
    tree['color'] = get_object_color(object_id, positions)
    tree['shape'] = get_object_shape(object_id, positions)
    tree['location'] = _get_initial_location(object_id, positions)
    return tree

def get_location(x,y):
    location = []

    # Middle of the board
    if 1 < x < 5 and 1 < y < 5:
        return LOCATION_FORMAT % (3.5, 3.5)

    # 4 corners of the board
    for coordinate in (x,y):
        if coordinate <= 3:
            location.append(0)
        else:
            location.append(7)
    return LOCATION_FORMAT % tuple(location)


"""
 Initial locations on the board (8x8):
"""
def _get_initial_location(object_id, positions):
    x = positions[object_id]['x'][0]
    y = positions[object_id]['y'][0]
    return get_location(x, y)


"""
Get the object location after it has been moved.
"""
def get_final_location(object_id, positions):
    x = positions[object_id]['x'][1]
    y = positions[object_id]['y'][1]
    return get_location(x, y)


def get_object_color(object_id, positions):
    color = positions[object_id]['F_HSV']
    if '-' in color:
        return 'color_'+color.split('-')[-1]
    return 'color_'+color


def get_object_shape(object_id, positions):
    return 'type_'+positions[object_id]['F_SHAPE']


"""
Calculate the initial and final distance between the moved object and the other objects
in the scene (except the gripper). 
return: [<x-initial>, <x-final>, <y-initial>, <y-final>] for every <static-moving> object pair in every scene
return: [<initial difference in x >, <final difference in x>, <initial difference in y>, <final difference in y>] 
for every <static-moving> object pair in every scene
"""
def _get_distances(positions):
    distances = []
    mov_obj = None
    for obj in positions:
        if obj != 'gripper':
            if positions[obj]['moving']:
                mov_obj = obj
                break
    if mov_obj != None:
        x1 = positions[mov_obj]['x']
        y1 = positions[mov_obj]['y']
        # z1 = positions[mov_obj]['z']
        for obj in positions:
            if obj != 'gripper' and obj != mov_obj:
                x2 = positions[obj]['x']
                y2 = positions[obj]['y']
                d = [np.abs(x1[0]-x2[0]),np.abs(x1[1]-x2[1]),np.abs(y1[0]-y2[0]),np.abs(y1[1]-y2[1])]
                for i in d:
                    if i not in distances:
                        distances.append(i)
    return distances


def get_distance_2d(object_1_id, object_2_id):
    x1 = positions[object_1_id]['x']
    y1 = positions[object_1_id]['y']
    x2 = positions[object_2_id]['x']
    y2 = positions[object_2_id]['y']
    d = [np.abs(x1[0] - x2[0]), np.abs(x1[1] - x2[1]), np.abs(y1[0] - y2[0]), np.abs(y1[1] - y2[1])]
    return d


def get_initial_relation_to(object_1_id, object_2_id, positions):
    x1 = positions[object_1_id]['x']
    y1 = positions[object_1_id]['y']
    z1 = positions[object_1_id]['z']
    x2 = positions[object_2_id]['x']
    y2 = positions[object_2_id]['y']
    z2 = positions[object_2_id]['z']
    # d = (np.abs(x1[0] - x2[0]), np.abs(y1[0] - y2[0]), np.abs(z1[0] - z2[0]))
    d = (x1[0] - x2[0], y1[0] - y2[0], z1[0] - z2[0])
    # Normalize the distances to either 1, -1 or 0
    d = tuple([int(e / abs(e)) if e != 0 else 0 for e in d])
    return d


def get_final_relation_to(object_1_id, object_2_id, positions):
    x1 = positions[object_1_id]['x']
    y1 = positions[object_1_id]['y']
    z1 = positions[object_1_id]['z']
    x2 = positions[object_2_id]['x']
    y2 = positions[object_2_id]['y']
    z2 = positions[object_2_id]['z']
    d = (x1[1] - x2[1], y1[1] - y2[1], z1[1] - z2[1])
    # Normalize the distances to either 1, -1 or 0
    d = tuple([int(e / abs(e)) if e != 0 else 0 for e in d])
    return d


"""
The relations that we have names for in natural language are usually those along a single axis:
for instance: 
    z-axis = above, under, on top of 
    x or y-axis = next to, opposite
For simplicity, any other relations will be filtered out.
"""
def validate_relation(relation):
    x,y,z = relation
    if x != 0:
        return y == 0 and z == 0
    if y != 0:
        return x == 0 and z == 0
    if z != 0:
        return x == 0 and y == 0


def get_all_elements_from_scene(event_tree):
    if event_tree == {}:
        logging.warning("Empty tree!")
        return {}

    features = {'actions':set(event_tree['actions']),
                'colors': set(),
                'shapes':set(),
                'locations':set(),
                'relations':set()
                }


    # Collect properties of all moving objects
    for entity_key in event_tree['entities']:
        shape, color = entity_key
        features['colors'].add(color)
        features['shapes'].add(shape)

        # initial location of each moving object
        features['locations'] = features['locations'].union(event_tree['entities'][entity_key]['locations'])

        # collect entity relations from the initial layout
        for relation in event_tree['entities'][entity_key]['relations']:
            features['relations'].add(relation)
            features['colors'].add(event_tree['entities'][entity_key]['relations'][relation]['color'])
            features['shapes'].add(event_tree['entities'][entity_key]['relations'][relation]['shape'])


    # add the destination of the moving objects
    features['locations'].add(event_tree['destination']['absolute'])

    # Collect properties of all relative destinations (determined by relation + object)
    for relation in event_tree['destination']['relative']:
        features['relations'].add(relation)
        features['colors'].add(event_tree['destination']['relative'][relation]['color'])
        features['shapes'].add(event_tree['destination']['relative'][relation]['shape'])

    return features

def get_all_colors_in_the_scene(positions):
    colors = set()
    for obj_id in positions:
        if obj_id != 'gripper':
            colors.add(get_object_color(obj_id, positions))
    return colors

def get_all_shapes_in_the_scene(positions):
    shapes = set()
    for obj_id in positions:
        if obj_id != 'gripper':
            shapes.add(get_object_shape(obj_id, positions))
    return shapes



for scene in range(1, 1001):
    print('Extracting features from scene : ',scene)

    positions = read_pickle(scene)

    tree = get_event_tree(positions)
    if tree != {}:
        logging.debug(tree['actions'])
        entity_key = list(tree['entities'].keys())[0]
        logging.debug(entity_key)
        logging.debug(tree['entities'][entity_key])
        logging.debug(tree['destination']['absolute'])
        logging.debug(tree['destination']['relative'])

    all_elements = get_all_elements_from_scene(tree)
    all_elements['colors'] = get_all_colors_in_the_scene(positions)
    all_elements['shapes'] = get_all_shapes_in_the_scene(positions)
    logging.debug(all_elements)
    print()

    pkl_file = DATA_FOLDER+'visual-features/'+str(scene)+'_vision_tree.p'
    pickle.dump(tree, open(pkl_file, 'wb'))
    pkl_file = DATA_FOLDER + 'visual-features/' + str(scene) + '_elements.p'
    pickle.dump(all_elements, open(pkl_file, 'wb'))

