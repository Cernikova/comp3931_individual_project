import pickle
from constants import *
from pickle_functions import *
import logging
import re

logging.basicConfig(format='%(message)s', level=logging.INFO)


def is_subtree(grammar_tree, grammar_subtree):
    match = re.match(' '.join(grammar_subtree), ' '.join(grammar_tree))
    if match:
        return True
    return False


def get_n_correct(grammar, rcl_tree):
    max_correct = 0
    grammar_length = len(grammar)
    rcl_length = len(rcl_tree)
    for i in range(abs(grammar_length - rcl_length) + 1):
        if grammar_length >= rcl_length:
            eval = zip(grammar[i:], rcl_tree)
            correct = len([(predicted, correct) for (predicted, correct) in eval if predicted == correct])
        else:
            eval = zip(grammar, rcl_tree[i:])
            correct = len([(predicted, correct) for (predicted, correct) in eval if predicted == correct])
        if correct > max_correct:
            max_correct = correct
    logging.debug('--------------%d----------------' % sentence_id)
    logging.debug(grammar)
    logging.debug(rcl_tree)
    total = max(len(grammar), len(rcl_tree))
    logging.debug("Total:%d Correct:%d " % (total, max_correct))
    return total, max_correct


total_sentences = 0
correct_sentences = 0
correct_subsets = 0
unidentified_grammar = 0
for scene in range(1, 1001):
    logging.info('**********************'+str(scene)+'****************************')
    sentences = read_sentences(scene)
    try:
        rcl_trees = read_rcl(scene)
    except:
        logging.warning("RCL not found")
        continue

    for sentence_id in rcl_trees:
        try:
            grammar = read_grammar_tree(sentence_id)
        except Exception as e:
            logging.warning("No grammar found. %s", e)
            unidentified_grammar += 1
            continue

        sentence_text = sentences[sentence_id]['text']

        # if sentence_id in bad_sentences:
        #     continue

        rcl_tree = rcl_trees[sentence_id]


        total, max_correct = get_n_correct(grammar, rcl_tree)
        correct_subsets += max_correct/total


        if max_correct == total: #or is_subtree(rcl_tree, grammar):
            correct_sentences += 1
        else:
            logging.info(sentence_text)
            logging.info("robot: %s \nrcl  : %s" % (str(grammar), str(rcl_tree)))

        total_sentences += 1

logging.info("\n**********************************")
logging.info("   Total:%d \n   Exact match:%d \n   Exact match ratio: %f" % (total_sentences, correct_sentences,
                                                           correct_sentences/total_sentences))
logging.info("   Partial match ratio: %f" % (correct_subsets/total_sentences))
logging.info("   Skipped: %d" % unidentified_grammar)
logging.info("**********************************")




