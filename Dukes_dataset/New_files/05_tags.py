import numpy as np
from constants import *
import pickle
from test_constants import GROUND_TRUTH_DICT, GROUND_TRUTH_DICT_SPANISH
import logging
from pickle_functions import *

logging.basicConfig(format='%(message)s', level=logging.INFO)


def get_concurrence_rate_dict(start=1, end=1001):
    visual_elements_superset = set()
    n_grams_superset = set()
    concurrence_dict = {}
    for scene in range(start, end):

        visual_elements_dict = read_visual_elements(scene)
        n_grams = read_n_grams(scene)

        # Collect features of all types (that is colors, shapes, locations, relations...) together in a single array
        visual_elements_list = []
        for element_type in visual_elements_dict:
            visual_elements_list.extend(visual_elements_dict[element_type])

        # Create a dictionary in the form {<n-gram>: 'total': <overall number of appearances>,
        # <visual feature 1>: <concurrent appearances>, <visual feature 1>: <concurrent appearances>, ...}
        for n_gram in n_grams:
            n_grams_superset.add(n_gram)
            if n_gram not in concurrence_dict:
                concurrence_dict[n_gram] = {'total': 0}
            concurrence_dict[n_gram]['total'] += 1
            for visual_feature in visual_elements_list:
                visual_elements_superset.add(visual_feature)
                if visual_feature not in concurrence_dict[n_gram]:
                    concurrence_dict[n_gram][visual_feature] = 0
                concurrence_dict[n_gram][visual_feature] += 1
    return concurrence_dict, n_grams_superset, visual_elements_superset



def dict_to_matrix(linguistic_visual_dict, all_n_grams, all_visual_elements):
    rows = len(all_visual_elements)
    cols = len(all_n_grams)

    cost_matrix = np.ones((rows, cols), dtype=np.float32)

    threshold = 10
    for col, n_gram in enumerate(sorted(all_n_grams)):
        for row, visual in enumerate(sorted(all_visual_elements)):
            if linguistic_visual_dict[n_gram].get(visual):
                if linguistic_visual_dict[n_gram]['total'] >= threshold:
                    coefficient = 1 - linguistic_visual_dict[n_gram][visual]/float(linguistic_visual_dict[n_gram]['total'])
                    cost_matrix[row,col] = coefficient

    return cost_matrix


def do_magic(cost_matrix):
    n_rows, n_cols = cost_matrix.shape

    # Hungarian algorithm
    for row in range(n_rows):
        cost_matrix[row, :] -= np.min(cost_matrix[row, :])
    for col in range(n_cols):
        cost_matrix[:, col] -= np.min(cost_matrix[:, col])
    return cost_matrix


def matrix_to_dict_by_ngrams(cost_matrix, all_n_grams, all_visual_elements, alpha=0.02):

    # Consider each value smaller or equal than alpha to be a hypothesis. Store them in the hypothesis dictionary
    sorted_n_grams = sorted(all_n_grams)
    sorted_visual = sorted(all_visual_elements)
    hypotheses_tags = {}
    for row, visual in enumerate(sorted_visual):
        for col, n_gram in enumerate(sorted_n_grams):
            if cost_matrix[row, col] <= alpha:
                if n_gram not in hypotheses_tags:
                    hypotheses_tags[n_gram] = {}
                hypotheses_tags[n_gram][visual] = 0

    return hypotheses_tags


def matrix_to_dict_by_visual(cost_matrix, all_n_grams, all_visual_elements, alpha=0.02):

    # Consider each value smaller or equal than alpha to be a hypothesis. Store them in the hypothesis dictionary
    sorted_n_grams = sorted(all_n_grams)
    sorted_visual = sorted(all_visual_elements)
    hypotheses_tags = {}

    for row, visual in enumerate(sorted_visual):
        for col, n_gram in enumerate(sorted_n_grams):
            if cost_matrix[row, col] <= alpha:
                if visual not in hypotheses_tags:
                    hypotheses_tags[visual] = {}
                hypotheses_tags[visual][n_gram] = 0

    return hypotheses_tags


def get_words_with_uncertain_meanings(hypotheses, limit=3):
    junk_ngrams = []
    for n_gram in hypotheses:
        if len(hypotheses[n_gram]) > limit:
            junk_ngrams.append(n_gram)
    return junk_ngrams


def remove_junk_ngrams(hypotheses, junk_ngrams):
    total = 0
    for visual in hypotheses:
        filtered = {}
        for n_gram in hypotheses[visual]:
            if n_gram not in junk_ngrams:
                filtered[n_gram] = hypotheses[visual][n_gram]
        hypotheses[visual] = filtered
        total += len(hypotheses[visual])

    return hypotheses


def evaluate(hypotheses, language = 'en'):
    if language =='en':
        GROUND_TRUTH = GROUND_TRUTH_DICT
    elif language == 'es':
        GROUND_TRUTH = GROUND_TRUTH_DICT_SPANISH
    total = 0
    correct = 0
    correct_hypotheses = {}
    for visual in hypotheses:
        correct_hypotheses[visual] = []
        for n_gram in hypotheses[visual]:
            if visual in GROUND_TRUTH and n_gram in GROUND_TRUTH[visual]:
                correct += 1
                correct_hypotheses[visual].append(n_gram)
        total += len(hypotheses[visual])
        logging.info("%s ----> %s" % (visual, str(correct_hypotheses[visual])))

    return total, correct


def refine_hypotheses(hyp_dict):
    for visual in hyp_dict:
        n_grams = set(hyp_dict[visual].keys())
        for n_gram in n_grams:
            parts = set(n_gram.split(' '))
            if len(parts) > 1:
                if parts.intersection(n_grams) == parts:
                    for part in parts:
                        if part in hyp_dict[visual]:
                            rm = hyp_dict[visual].pop(part)
                elif len(parts.intersection(n_grams))>0:
                    rm = hyp_dict[visual].pop(n_gram)
                    if visual == 'action_type_2' and n_gram=='move':
                        logging.info("n-gram: %s  intersection: %s" % (n_gram, parts.intersection(n_grams)))


def add_different_word_forms(hyp_dict, inflections):
    visual_elements = list(hyp_dict.keys())
    for visual_element in visual_elements:
        n_grams = list(hyp_dict[visual_element].keys())
        for n_gram in n_grams:
            for word_root in inflections:
                if n_gram.startswith(word_root):
                    if n_gram in inflections[word_root]:
                        for inflection in inflections[word_root]:
                            if inflection not in hyp_dict[visual_element]:
                                hyp_dict[visual_element][inflection] = 0


def filter_by_rules(hyp_dict):
    redundant = {}
    for visual in hyp_dict:
        redundant[visual] = set()
        for n_gram in hyp_dict[visual]:
            if ' ' in n_gram:
                words = n_gram.split()
                all_present = True
                one_present = False
                for word in words:
                    if word not in hyp_dict[visual]:
                        all_present = False
                    else:
                        one_present = True
                if all_present:
                    redundant[visual] = redundant[visual].union(set(words))
                elif not all_present and one_present:
                    redundant[visual].add(n_gram)

    for visual in redundant:
        for n_gram in redundant[visual]:
            hyp_dict[visual].pop(n_gram)




logging.info("Calculating the coefficient of concurrent appearances of each n_gram and visual element.\n")
concurrence_dict, n_grams_superset, visual_elements_superset = get_concurrence_rate_dict(1, 1001)

logging.info("Checking the validity of each grounding.\n")
cost_matrix = dict_to_matrix(concurrence_dict, n_grams_superset, visual_elements_superset)
cost_matrix = do_magic(cost_matrix)
hypotheses_by_ngram = matrix_to_dict_by_ngrams(cost_matrix, n_grams_superset, visual_elements_superset, 0.02)
hypotheses_by_visual = matrix_to_dict_by_visual(cost_matrix, n_grams_superset, visual_elements_superset, 0.02)
total, correct = evaluate(hypotheses_by_visual, language='es')
logging.info("Current number: %d, correct %d" % (total, correct))

logging.info("\nRefining hypotheses.\n")

# Filter out n-gram with more than 3 potential meanings
junk_ngrams = get_words_with_uncertain_meanings(hypotheses_by_ngram, 3)
hypotheses_by_visual = remove_junk_ngrams(hypotheses_by_visual, junk_ngrams)
total, correct = evaluate(hypotheses_by_visual, language='es')
logging.info("Current number: %d, correct %d" % (total, correct))


logging.info("\nAdding all word forms")
inflections = read_inflections()
add_different_word_forms(hypotheses_by_visual, inflections)
total, correct = evaluate(hypotheses_by_visual, language='es')
logging.info("Current number: %d, correct %d" % (total, correct))


pkl_file = DATA_FOLDER+'groundings/tags_by_ngram.p'
pickle.dump(hypotheses_by_ngram, open(pkl_file, 'wb'))

pkl_file = DATA_FOLDER+'groundings/tags_by_visual.p'
pickle.dump(hypotheses_by_visual, open(pkl_file, 'wb'))



