from googletrans import Translator
import logging
from constants import *
import pickle
import time
from google_trans_new import google_translator
from random import randint

def _read_sentences(scene):
    pkl_file = DATA_FOLDER+'scenes/'+str(scene)+'_sentences.p'
    data = open(pkl_file, 'rb')
    sentences = pickle.load(data)
    return sentences

logging.basicConfig(format='%(message)s', level=logging.INFO)


def translate_sentences(scene, dest_language, wait_time = 4):
    t = Translator(service_urls=['translate.googleapis.com'])
    batch = []
    translated_sentences = {}
    sentence_ids = []

    sentences = _read_sentences(scene)

    for sentence_id in sentences:
        sentence_text = sentences[sentence_id]['text']
        sentence_ids.append(sentence_id)
        batch.append(sentence_text+'!')
    translations = t.translate(batch, src='en', dest=dest_language)
    text_file = open(DATA_FOLDER + str(scene) + '_sentences' + '.txt', 'w')
    for index, translation in enumerate(translations):
        if translation.text == batch[0]:
            logging.error("Your IP was blocked!!")
            return 0
        translation_text = translation.text.replace('!', '').replace('¡', '')
        translated_sentences[sentence_ids[index]] = sentences[sentence_ids[index]]
        translated_sentences[sentence_ids[index]]['text'] = translation_text
        print(sentence_ids[index], translation_text)
        text_file.write(translation_text + '\n')
    text_file.close()
    pkl_file = DATA_FOLDER + 'scenes/' + str(scene) + '_sentences.p'
    pickle.dump(translated_sentences, open(pkl_file, 'wb'))
    return 1

def replace_symbols(text):
    for character in text:
        if not character.isalpha() and not character.isspace():
            text = text.replace(character, '')
    return text



t = google_translator()


def translate_sentences_new(scene, dest_language, wait_time = 4):
    # t = google_translator()
    batch = []
    translated_sentences = {}
    sentence_ids = []

    sentences = _read_sentences(scene)

    for sentence_id in sentences:
        sentence_text = sentences[sentence_id]['text']
        if 'can' in sentence_text:
            sentence_text = sentence_text.replace('can', 'tin')
        sentence_ids.append(sentence_id)
        batch.append(sentence_text+'!')

    if not batch:
        logging.warning("Missing sentences!")
        text_file = open(DATA_FOLDER + 'scenes/' + str(scene) + '_sentences' + '.txt', 'w')
        text_file.close()
        translated_sentences = {}
        pkl_file = DATA_FOLDER + 'scenes/' + str(scene) + '_sentences.p'
        pickle.dump(translated_sentences, open(pkl_file, 'wb'))
        return 1

    translations = t.translate('; '.join(batch), lang_src='en', lang_tgt=dest_language)
    if translations == str(batch):
        logging.error('Your IP was blocked yet again!')
        return 0
    text_file = open(DATA_FOLDER + 'scenes/' + str(scene) + '_sentences' + '.txt', 'w')
    for index, translation in enumerate(translations.split(';')):
        translation_text = translation.lower().strip()
        translation_text = replace_symbols(translation_text)
        translation_text = translation_text.replace('elija', 'recoge')
        translation_text = translation_text.replace('elige', 'recoge')
        translated_sentences[sentence_ids[index]] = sentences[sentence_ids[index]]
        translated_sentences[sentence_ids[index]]['text'] = translation_text
        print(sentence_ids[index], translation_text)
        text_file.write(translation_text + '\n')
    text_file.close()
    pkl_file = DATA_FOLDER + 'scenes/' + str(scene) + '_sentences.p'
    pickle.dump(translated_sentences, open(pkl_file, 'wb'))
    return 1



def translate_sentence(sentence_text, dest_language):
    t = Translator(service_urls=['translate.googleapis.com'])

    sentence_translation = t.translate(sentence_text, src='en', dest=dest_language)
    logging.debug(sentence_translation.text)
    return sentence_translation.text

def translate_sentence_new(sentence, dest_lang):
    translator = google_translator()
    translate_text = translator.translate(sentence, lang_src='en', lang_tgt=dest_lang)
    logging.debug(translate_text)
    time.sleep(5)

    return translate_text



for scene in range(900, 1001):
    logging.info("******************* scene %d ***************" % scene)
    translate_sentences_new(scene, 'es')
    time.sleep(randint(8, 12))


