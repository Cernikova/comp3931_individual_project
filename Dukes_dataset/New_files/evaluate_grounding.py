from constants import *
import pickle
from test_constants import GROUND_TRUTH_DICT, GROUND_TRUTH_DICT_SPANISH
from pickle_functions import *
import logging

logging.basicConfig(format='%(message)s', level=logging.INFO)


def reverse_tags(tags, feature):
    reversed_dict = {}
    for visual_element in tags:
        if visual_element.startswith(feature):
            for n_gram in tags[visual_element]:
                if n_gram not in reversed_dict:
                    reversed_dict[n_gram] = {}
                reversed_dict[n_gram][visual_element] = tags[visual_element][n_gram]
    return reversed_dict


def get_groups(language = "en"):
    if language == 'en':
        GT = GROUND_TRUTH_DICT
    elif language == 'es':
        GT = GROUND_TRUTH_DICT_SPANISH

    group_dict = {}

    for visual in GT:
        feature_group = visual.split('_')[0]
        feature_group = feature_group+'s'
        if feature_group not in group_dict:
            group_dict[feature_group] = set()
        group_dict[feature_group] = group_dict[feature_group].union(set(GT[visual]))
    return group_dict


def get_all_ngrams():
    all_ngrams = set()
    for scene in range(1,1001):
        pkl_file = DATA_FOLDER + 'learning/' + str(scene) + '_linguistic_features.p'
        data = open(pkl_file, 'rb')
        lf = pickle.load(data)['n_grams']
        all_ngrams = all_ngrams.union(lf)

    stop_words = set(read_stop_words())
    all_ngrams = stop_words.union(all_ngrams)

    return all_ngrams


def calculate_f1(feature, language='en'):

    logging.info("\n *********** Feature %s ****************" % feature)

    if language == 'en':
        GT = GROUND_TRUTH_DICT
    elif language == 'es':
        GT = GROUND_TRUTH_DICT_SPANISH


    passed_tags_by_visual = read_passed_tags()
    passed_tags = reverse_tags(passed_tags_by_visual, feature)
    ground_truth = {}
    for visual in GT:
        if visual.startswith(feature):
            for n_gram in GT[visual]:
                if n_gram not in ground_truth:
                    ground_truth[n_gram] = set()
                ground_truth[n_gram].add(visual)


    all_words = get_all_ngrams()
    stop_words = read_stop_words()
    all_words = all_words.union(set(stop_words))

    if feature == '':
        options = set()
        feature_space_options = get_groups(language=language)
        for feature_space in feature_space_options:
            options = options.union(feature_space_options[feature_space])
    else:
        options = get_groups(language=language)[feature + 's']

    options = all_words.intersection(set(options))
    logging.debug(options)


    correct_tags = {}
    incorrect_tags = {}
    unidentified_tags = {}
    n_correct = 0
    n_incorrect = 0
    n_unidentified = 0


    for n_gram in ground_truth:
        if n_gram not in options:
            continue
        actual = ground_truth[n_gram]
        if n_gram not in passed_tags:
            predicted = set()
        else:
            predicted = set(passed_tags[n_gram])
        correct_tags[n_gram] = predicted.intersection(actual)
        n_correct += len(correct_tags[n_gram])
        incorrect_tags[n_gram] = predicted - predicted.intersection(actual)
        n_incorrect += len(incorrect_tags[n_gram])
        unidentified_tags[n_gram] = actual - predicted.intersection(actual)
        n_unidentified += len(unidentified_tags[n_gram])
        if len(incorrect_tags[n_gram]) == 0:
            incorrect_tags.pop(n_gram)
        if len(unidentified_tags[n_gram]) == 0:
            unidentified_tags.pop(n_gram)
        if len(correct_tags[n_gram]) == 0:
            correct_tags.pop(n_gram)


    logging.debug("\ncorrect:\n%s" % str(correct_tags))
    logging.debug("\nincorrect\n%s" % str(incorrect_tags))
    logging.debug("\nunidentified\n%s" % str(unidentified_tags))


    TP = n_correct
    FP = n_incorrect
    FN = n_unidentified

    logging.debug("TP: %d   FP:%d   FN:%d" % (TP, FP, FN))

    if TP!=0:
        precision = TP/(TP+FP)
        recall = TP/(TP+FN)

        F1 = 2*precision*recall/(precision+recall)
        logging.debug("F1 score: %f" % F1)
        return F1
    else:
        logging.warning("Error!! No true positives were found!")
        return 0


def evaluate(language='en'):
    print(calculate_f1('', language=language))
    print(calculate_f1('action', language=language))
    print(calculate_f1('type', language=language))
    print(calculate_f1('color', language=language))
    print(calculate_f1('relation', language=language))
    print(calculate_f1('location', language=language))

evaluate('es')


