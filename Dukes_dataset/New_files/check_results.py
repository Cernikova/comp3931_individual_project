import pickle
from constants import *
import operator
import itertools

def read_tags():
    pkl_file = DATA_FOLDER+'groundings/tags_by_visual.p'
    data = open(pkl_file, 'rb')
    tags = pickle.load(data)
    return tags

def check_subsets(hyp_dict):
    permutations = itertools.product(hyp_dict.keys(), hyp_dict.keys())
    # Get superset-subset pairs of words, such as ('green on', 'green')
    subsets = [p for p in permutations if p[1] in p[0].split(' ')]
    for s in subsets:
        # In case the subset was already removed
        if s[0] in hyp_dict:
            # Check the frequency
            if hyp_dict[s[1]]/3 > hyp_dict[s[0]]:
                hyp_dict[s[1]] += hyp_dict.pop(s[0])
    return hyp_dict


def print_tags(tags, quota=100):
    for visual in tags:
        if tags[visual] == {}:
            continue
        # max_votes_ngram = max(tags[visual].items(), key=operator.itemgetter(1))[0]
        # max_votes = tags[visual][max_votes_ngram]
        # print(visual, " ----> ", [n + ': ' + str(tags[visual][n]) for n in tags[visual] if tags[visual][n] >= min(quota, max_votes)])
        hyps_sorted = sorted(tags[visual].items(), key=lambda item: item[1], reverse=True)[:5]
        # median = hyps_sorted[int(len(hyps_sorted)/2)]
        print(visual, " ------> ", hyps_sorted)


tags = read_tags()

print_tags(tags)

for visual in tags:
    if tags[visual] == {}:
        continue
    tags[visual] = check_subsets(tags[visual])

print("\n--------Refined--------")

print_tags(tags)




