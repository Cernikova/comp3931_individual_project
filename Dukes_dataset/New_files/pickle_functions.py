import pickle
from constants import *


def read_visual_elements(scene):
    pkl_file = DATA_FOLDER+'visual-features/'+str(scene)+'_elements.p'
    data = open(pkl_file, 'rb')
    elements = pickle.load(data)
    return elements


def read_vision_tree(scene):
    pkl_file = DATA_FOLDER+'visual-features/'+str(scene)+'_vision_tree.p'
    data = open(pkl_file, 'rb')
    tree = pickle.load(data)
    return tree


def read_stop_words():
    pkl_file = DATA_FOLDER+'learning/stop_words.p'
    data = open(pkl_file, 'rb')
    stop = pickle.load(data)
    return stop


def read_function_words():
    pkl_file = DATA_FOLDER+'learning/function_words.p'
    data = open(pkl_file, 'rb')
    stop = pickle.load(data)
    return stop


def read_inflections():
    pkl_file = DATA_FOLDER + 'learning/inflections.p'
    data = open(pkl_file, 'rb')
    n_grams = pickle.load(data)
    return n_grams


def read_n_grams(scene):
    pkl_file = DATA_FOLDER + 'learning/' + str(scene) + '_linguistic_features.p'
    data = open(pkl_file, 'rb')
    n_grams = pickle.load(data)['n_grams']
    return n_grams


def read_sentences (scene):
    pkl_file = DATA_FOLDER+'scenes/'+str(scene)+'_sentences.p'
    data = open(pkl_file, 'rb')
    sentences = pickle.load(data)
    return sentences


def read_tags():
    pkl_file = DATA_FOLDER+'groundings/tags_by_visual.p'
    data = open(pkl_file, 'rb')
    tags = pickle.load(data)
    return tags


def read_passed_tags():
    pkl_file = DATA_FOLDER+'groundings/passed_tags_by_visual.p'
    data = open(pkl_file, 'rb')
    tags = pickle.load(data)
    return tags


def read_rcl_trees(scene):
    pkl_file = DATA_FOLDER+'scenes/'+str(scene)+'_rcl.p'
    data = open(pkl_file, 'rb')
    rcl_trees = pickle.load(data)
    return rcl_trees


def read_grammar_tree(sentence_id):
    pkl_file = DATA_FOLDER+'grammar/'+str(sentence_id)+'_grammar_tree.p'
    data = open(pkl_file, 'rb')
    tree = pickle.load(data)
    return tree


def read_rcl(scene):
    pkl_file = DATA_FOLDER+'rcl/'+str(scene)+'_rcl_grammar.p'
    data = open(pkl_file, 'rb')
    tree = pickle.load(data)
    return tree
