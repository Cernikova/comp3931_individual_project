import pickle
from constants import *
import re
import logging
import operator
import itertools
from pickle_functions import *

logging.basicConfig(format='%(message)s', level=logging.INFO)


def reverse_tags(tags):
    reversed_dict = {}
    for visual_element in tags:
        for n_gram in tags[visual_element]:
            if n_gram not in reversed_dict:
                reversed_dict[n_gram] = {}
            reversed_dict[n_gram][visual_element] = tags[visual_element][n_gram]
    return reversed_dict


def increase_votes(feature_representations, n_gram, inflections):
    word_root_found = False
    for word_root in inflections:
        if word_root_found:
            break
        if n_gram.startswith(word_root):
            if n_gram in inflections[word_root]:
                word_root_found = True
                for inflection in inflections[word_root]:
                    if inflection in feature_representations:
                        feature_representations[inflection] += 1
                    else:
                        feature_representations[inflection] = 1
    # The robot does not recognise any other forms of this word.
    if not word_root_found:
        feature_representations[n_gram] += 1


def find_max_matches(matched_sentences):
    max_match = 0
    for match in matched_sentences:
        n_matched_features = len(match.split(';'))
        if n_matched_features > max_match:
            max_match = n_matched_features
    return max_match


def test_related_entities(sentences_to_test, vision_subtree, tags):
    all_options = sentences_to_test
    missing = set()
    for relation in vision_subtree:
        # if there is an n-gram representing the relation
        if relation in tags:
            entity_shape = vision_subtree[relation]['shape']
            entity_color = vision_subtree[relation]['color']
            # if '-' in entity_color:
            #     entity_color = 'color_'+ entity_color.split('-')[-1]
            feature_options = {entity_shape:tags[entity_shape],
                               entity_color:tags[entity_color],
                               relation:tags[relation]
                               }
            entity_found = False
            for visual_feature in feature_options:
                feature_found = False
                feature_representations = feature_options[visual_feature]
                if visual_feature == relation and not entity_found:
                    # print(relation, entity_shape, entity_color)
                    continue
                new_options = {}
                for match in all_options:
                    for n_gram in feature_representations:
                        matched_sentence = all_options[match]
                        matched_sentence, n = re.subn(' '+ n_gram +' ', ' [' + visual_feature + '] ', matched_sentence, 1)
                        if n > 0:
                            # increase_votes(feature_representations, n_gram)
                            new_options[match + ';' + visual_feature+':'+n_gram] = matched_sentence
                            entity_found = True
                            feature_found = True
                if len(new_options) > 0:
                    all_options = new_options
            if not feature_found and entity_found:
                missing.add(visual_feature)

    return all_options, missing


def test_level_1(feature_options, current_sentence):
    missing = set()
    # Start from the feature that the least hypothetical names.
    feature_options = dict(sorted(feature_options.items(), key=lambda item: len(item[1])))
    matched_sentences = {}
    for visual_feature in feature_options:
        found = False
        feature_representations = feature_options[visual_feature]
        if not matched_sentences:
            # matched_sentences = {}
            for n_gram in feature_representations:
                matched_sentence = current_sentence
                matched_sentence, n = re.subn(' ' + n_gram + ' ', ' [' + visual_feature + '] ', matched_sentence, 1)
                if n > 0:
                    # increase_votes(feature_representations, n_gram)
                    matched_sentences[visual_feature+':'+n_gram] = matched_sentence
                    found = True
        else:
            new_options = {}
            for match in matched_sentences:
                for n_gram in feature_representations:
                    # print(n_gram)
                    matched_sentence = matched_sentences[match]
                    matched_sentence, n = re.subn(' ' + n_gram + ' ', ' [' + visual_feature + '] ', matched_sentence, 1)
                    if n > 0:
                        # increase_votes(feature_representations, n_gram)
                        new_options[match + ';' + visual_feature+':'+n_gram] = matched_sentence
                        found = True
            if len(new_options) > 0:
                matched_sentences = new_options
        if not found:
            missing.add(visual_feature)

    return matched_sentences, missing


def remove_stopwords(sentence, stop_words):
    for stop_word in stop_words:
        sentence = re.sub(' '+stop_word+' ', ' ', sentence)
    return sentence


def get_grammar_old(sentence):
    features_in_sentence = re.findall(r'\[(.*?)\]', sentence)
    grammar_tree = tuple([f.split('_')[0] for f in features_in_sentence])
    return grammar_tree


def get_grammar(sentence):
    sentence = re.sub(r', ', ',', sentence)
    elements = re.split('\[|\]| ', sentence)
    grammar_tree = [element.strip().split("_")[0] for element in elements if element != '']
    for i, g in enumerate(grammar_tree):
        if g in function_words:
            grammar_tree[i] = 'F'
    grammar_tree = tuple(grammar_tree)

    return grammar_tree


def generate_grammar(sentence_matches):
    grammar_trees = {}
    for match in sentence_matches:
        grammar_tree = get_grammar(sentence_matches[match])
        grammar_trees[grammar_tree] = {sentence_id}
    for grammar_tree in grammar_trees:
        logging.debug(grammar_tree)
    return grammar_trees


def get_first_entity_key(event_tree):
    return list(event_tree['entities'].keys())[0]


def get_level_one_feature_options(event_tree):
    entity_key = get_first_entity_key(event_tree)
    entity_shape, entity_color = entity_key
    actions = tree['actions']
    destination = tree['destination']['absolute']

    feature_options = {entity_shape: tags[entity_shape],
                       entity_color: tags[entity_color],
                       destination: tags[destination]}

    for action in actions:
        feature_options[action] = tags[action]

    return feature_options


def update_grammar_trees_dict(all_grammar_trees, current_grammar_trees):
    for grammar_tree in current_grammar_trees:
        # If the grammar tree was not observed yet, register it
        if grammar_tree not in all_grammar_trees:
            all_grammar_trees[grammar_tree] = current_grammar_trees[grammar_tree]
        # If the grammar tree was observed before, update the number of votes
        else:
            all_grammar_trees[grammar_tree] = all_grammar_trees[grammar_tree].union(current_grammar_trees[grammar_tree])


def get_top_grammar_trees(n_top):
    top_trees = []
    for t in sorted(all_grammar_trees.items(), key=lambda item: len(item[1]), reverse=True)[:n_top]:
        top_trees.append(t[0])
    return top_trees


def is_subtree(grammar_tree, grammar_subtree):
    match = re.match(' '.join(grammar_subtree), ' '.join(grammar_tree))
    if match:
        return True
    return False


def filter_matched_sentences_by_subtrees(matched_sentences, subtrees):
    passed_sentences = {}
    subtrees = sorted(subtrees, key=len)
    longest_match = 0
    for subtree in subtrees:
        if len(subtree) < longest_match:
            break
        for match in matched_sentences:
            grammar_tree = get_grammar(matched_sentences[match])
            if len(grammar_tree) >= len(subtree) and is_subtree(grammar_tree, subtree):
                longest_match = len(subtree)
                # Copy the grammar tree along with its number of votes
                passed_sentences[match] = matched_sentences[match]
            elif is_subtree(subtree, grammar_tree):
                passed_sentences[match] = matched_sentences[match]

    if not longest_match:
        return matched_sentences

    return passed_sentences


def filter_matched_sentences_by_subtrees_old(matched_sentences, subtrees):
    passed_sentences = {}
    for match in matched_sentences:
        passed = False
        grammar_tree = get_grammar_old(matched_sentences[match])
        for subtree in subtrees:
            if len(grammar_tree) >= len(subtree) and is_subtree(grammar_tree, subtree):
                passed = True
                break
            elif is_subtree(subtree, grammar_tree):
                passed = True
                break
        if passed:
            passed_sentences[match] = matched_sentences[match]
    return passed_sentences


def filter_matched_sentences_by_remainder(matched_sentences):
    passed_sentences = {}
    remainders = {}
    for match in matched_sentences:
        grammar_tree = get_grammar(matched_sentences[match])
        remainder = list(filter(lambda a: a not in leaf_elements, grammar_tree))
        remainders[match] = len(remainder)
    sorted_matches = sorted(remainders, key=lambda m: remainders[m])
    if sorted_matches:
        min_remainder = remainders[sorted_matches[0]]
        for match in sorted_matches:
            if remainders[match] == min_remainder:
                passed_sentences[match] = matched_sentences[match]
            else:
                break

    return passed_sentences


def remove_supersets_from_hypotheses(hyp_dict):
    permutations = itertools.product(hyp_dict.keys(), hyp_dict.keys())
    # Get superset-subset pairs of words, such as ('green on', 'green')
    subsets = [p for p in permutations if p[1] in p[0].split(' ')]
    for s in subsets:
        # In case the subset was already removed
        if s[0] in hyp_dict:
            # Check the frequency
            if hyp_dict[s[1]]/2 > hyp_dict[s[0]]:
                hyp_dict[s[1]] += hyp_dict.pop(s[0])
    return hyp_dict


def update_grounding_votes(matched_sentences, tags, inflections):
    for match in matched_sentences:
        # print(match)
        matching_pairs = match.split(';')
        for matching_pair in matching_pairs:
            # print(matching_pair)
            visual_feature, n_gram = matching_pair.split(':')
            increase_votes(tags[visual_feature], n_gram, inflections)


def clear_tag_scores(tags):
    for visual_feature in tags:
        for n_gram in tags[visual_feature]:
            tags[visual_feature][n_gram] = 0


def update_missing_elements(all_missing, new_missing, sentence_id):
    for element in new_missing:
        if element not in all_missing:
            all_missing[element] = set()
        all_missing[element].add(sentence_id)


def generate_new_concepts(tags, diff):
    n = 0
    while 'new_' + str(n) in tags:
        if diff[0] in tags['new_' + str(n)] or diff[0] in tags['new_' + str(n)]:
            break
        n += 1
    new_concept = 'new_' + str(n)
    if new_concept not in tags:
        tags[new_concept] = {}
    if diff[0] not in tags[new_concept]:
        tags[new_concept][diff[0]] = 0
    if diff[1] not in tags[new_concept]:
        tags[new_concept][diff[0]] = 0
    tags['new_' + str(n)][diff[0]] = 1
    tags['new_' + str(n)][diff[1]] = 1
    print(tags['new_' + str(n)])


def induce_meanings(top_trees, grammar_trees, missing_elements, tags):
    for grammar1 in top_trees:
        for grammar2 in grammar_trees:
            if grammar1 == grammar2:
                continue
            if len(grammar1) != len(grammar2):
                continue
            differences = []
            for index in range(len(grammar1)):
                if grammar1[index] != grammar2[index]:
                    diff = (grammar1[index], grammar2[index])
                    if diff not in differences and diff[1] != 'F':
                        differences.append(diff)
            if len(differences) == 1:
                for diff in differences:
                    if diff[0] in leaf_elements and diff[1] not in leaf_elements:
                        category, n_gram = diff[0], diff[1]
                    else:
                        continue

                    for element in missing_elements:
                        if element.startswith(category):
                            # get sentences where the element is missing
                            sentence_ids_element = missing_elements[element]
                            # get sentences where the word has not been identified
                            sentence_ids_grammar = grammar_trees[grammar2]
                            if len(sentence_ids_grammar) > 1 and sentence_ids_grammar - sentence_ids_element == set():
                                if n_gram not in tags[element]:
                                    tags[element][n_gram] = 0
                                    increase_votes(tags[element], n_gram, inflections)
                                    logging.info("%s ------------> %s" % (element, n_gram))
                                break



tags = read_tags()
all_grammar_trees = {}
stop_words = read_stop_words()
function_words = read_function_words()
inflections = read_inflections()
learned_grammar = set()
all_folds_grammar = {}
new_concepts = {}

# The top n sentences will ba saved and used for the next iteration.
n = [5, 3, 10, 10, 20, 20]

for count, sentence_len in enumerate([(1, 5), (1, 5), (6, 15), (6, 15), (16, 100), (16, 100)][:6], 1):
    logging.info("\n\n******************************   NEW SECTION     ******************************\n\n")
    logging.info("Using gammar: "+str(learned_grammar))
    first_pass = count % 2
    all_missing = {}
    for scene in range(1, 1001):
        logging.info('\n')
        logging.info('************************************ scene '+str(scene)+' ****************************************')

        tree = read_vision_tree(scene)
        if tree == {}:
            logging.warning('Empty vision tree!')
            continue
        entity_key = get_first_entity_key(tree)
        relative_destinations = tree['destination']['relative']
        entity_relations = tree['entities'][entity_key]['relations']

        level_1_feature_options = get_level_one_feature_options(tree)

        sentences = read_sentences(scene)
        for sentence_id in sentences:
            current_sentence = ' '+sentences[sentence_id]['text']+' '
            # Uncomment to remove stopwords from the sentece
            # current_sentence = remove_stopwords(current_sentence, stop_words)

            current_sentence_length = len(current_sentence.strip().split(' '))
            if current_sentence_length < sentence_len[0] or current_sentence_length > sentence_len[1]:
                continue

            logging.debug('------------ ' + str(sentence_id) + ' ----------------')
            logging.debug(current_sentence)

            matched_sentences, missing = test_level_1(level_1_feature_options, current_sentence)

            update_missing_elements(all_missing, missing, sentence_id)

            if learned_grammar:
                matched_sentences = filter_matched_sentences_by_subtrees(matched_sentences, learned_grammar)
            matched_sentences = filter_matched_sentences_by_remainder(matched_sentences)


            max_matches = find_max_matches(matched_sentences)

            if max_matches < current_sentence_length:
                if relative_destinations:
                    matched_sentences, missing = test_related_entities(matched_sentences, relative_destinations, tags)
                    update_missing_elements(all_missing, missing, sentence_id)
                if learned_grammar:
                    matched_sentences = filter_matched_sentences_by_subtrees(matched_sentences, learned_grammar)
                matched_sentences = filter_matched_sentences_by_remainder(matched_sentences)

                if entity_relations:
                    matched_sentences, missing = test_related_entities(matched_sentences, entity_relations, tags)
                    update_missing_elements(all_missing, missing, sentence_id)
                if learned_grammar:
                    matched_sentences = filter_matched_sentences_by_subtrees(matched_sentences, learned_grammar)
                matched_sentences = filter_matched_sentences_by_remainder(matched_sentences)

            update_grounding_votes(matched_sentences, tags, inflections)

            max_matches = find_max_matches(matched_sentences)
            logging.debug("identified n-grams: %d" % max_matches)

            grammar_trees = generate_grammar(matched_sentences)
            if grammar_trees:
                top_grammar = max(grammar_trees.items(), key=operator.itemgetter(1))[0]
                logging.debug(top_grammar)
                final_grammar = list(filter(lambda a: a in leaf_elements, top_grammar))
                if not first_pass:
                    pkl_file = DATA_FOLDER + 'grammar/'+str(sentence_id)+'_grammar_tree.p'
                    pickle.dump(final_grammar, open(pkl_file, 'wb'))

                # Add the grammar trees generated from this sentence to all grammar trees from the current fold.
                update_grammar_trees_dict(all_grammar_trees, grammar_trees)
            else:
                logging.warning('No grammar trees were found!')
                if not first_pass:
                    pkl_file = DATA_FOLDER + 'grammar/' + str(sentence_id) + '_grammar_tree.p'
                    pickle.dump([], open(pkl_file, 'wb'))


    n_trees = n.pop(0)
    learned_grammar = set(get_top_grammar_trees(n_trees))
    all_folds_grammar[sentence_len] = learned_grammar

    used_elements = set()
    for t in learned_grammar:
        g_elements = [e for e in t if e in leaf_elements]
        used_elements = used_elements.union(g_elements)


    for visual in tags:
        if tags[visual] == {}:
            continue
        remove_supersets_from_hypotheses(tags[visual])
        for category in used_elements:
            if visual.startswith(category):
                filtered = {}
                for n_gram in tags[visual]:
                    if tags[visual][n_gram] > 0:
                        filtered[n_gram] = tags[visual][n_gram]
                    else:
                        logging.info("removing %s ---> %s" % (visual, n_gram))
                tags[visual] = filtered
        top_hypoteses = dict(sorted(tags[visual].items(), key=lambda item: item[1], reverse=True)[:10])
        logging.info("%s ----> %s", visual, str(top_hypoteses))


    if first_pass:
        induce_meanings(learned_grammar, all_grammar_trees, all_missing, tags)
        clear_tag_scores(tags)
        first_pass = False




for sentence_length in all_folds_grammar:
    print('*******', sentence_length, '********')
    for grammar_tree in all_folds_grammar[sentence_length]:
        print(grammar_tree)

pkl_file = DATA_FOLDER+'groundings/passed_tags_by_visual.p'
pickle.dump(tags, open(pkl_file, 'wb'))


