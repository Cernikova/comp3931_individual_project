import numpy as np
from constants import *
import pickle
import operator
from stemmer import stem
import itertools
from pickle_functions import *


def reverse_stem_ditc(stem_dict):
    reversed_dict = {}
    for suffix in stem_dict:
        for word_root in stem_dict[suffix]:
            if word_root not in reversed_dict:
                reversed_dict[word_root] = set()
            reversed_dict[word_root].add(suffix)
    return reversed_dict


def get_inflections(all_words):
    suffix_dict = {}
    word_combinations = itertools.combinations(all_words, 2)
    for comb in word_combinations:
        if '' in comb:
            continue
        word_root, suffixes = stem(comb[0], comb[1])
        if word_root:
            for suffix in suffixes:
                if not suffix in suffix_dict:
                    suffix_dict[suffix] = set()
                suffix_dict[suffix].add(word_root)

    valid_suffixes = {}
    for suffix in sorted(suffix_dict, key=len):
        if len(suffix_dict[suffix]) > 10:
            print(suffix, "----->", suffix_dict[suffix])
            valid_suffixes[suffix] = suffix_dict[suffix]

    print()
    word_roots = reverse_stem_ditc(valid_suffixes)
    valid_roots = {}
    for word_root in sorted(word_roots, key=len):
        if len(word_roots[word_root]) > 1:
            valid_roots[word_root] = [word_root+suffix for suffix in word_roots[word_root]]
            print(word_root, "----->", valid_roots[word_root])

    return valid_roots

"""
Document frequency dictionary: {<term>:<frequency>} 
"""
idf = {}
n_doc = 0.0
for scene in range(1,1001):
    print('extracting feature from scene : ',scene)
    pkl_file = DATA_FOLDER+'scenes/'+str(scene)+'_linguistic_features.p'
    sentences = read_sentences(scene)
    for sentence_id in sentences:
        n_doc += 1
        words = sentences[sentence_id]['text'].split(' ')

        # Increase term frequency or add a new term into dictionary.
        for word in words:
            if word not in idf:
                idf[word] = 1.0
            else:
                idf[word] += 1

all_words = idf.keys()

inflections = get_inflections(all_words)


"""
Sort terms by frequency.
"""
sorted_x = sorted(idf.items(), key=operator.itemgetter(1))
print(sorted_x)


"""
Filter out words with too high/low document frequency.
"""
x = idf
stopwords = []
function_words = []
# Use .2 for English and .9 for Spanish
alpha_min = .9
alpha_max = np.log(n_doc/23.0)
for word in idf:
    idf[word] = np.log(n_doc/idf[word])
    if idf[word] < alpha_min:
        function_words.append(word)
        stopwords.append(word)
    elif idf[word] > alpha_max:
        stopwords.append(word)


for word in stopwords.copy():
    not_stopword = False
    for word_root in inflections:
        if word.startswith(word_root):
            if word in inflections[word_root]:
                for inflection in inflections[word_root]:
                        if inflection not in stopwords:
                            not_stopword = True
                if not_stopword:
                    stopwords.remove(word)
                    print('removing ', word, ' from stop words')
                    break

print(function_words)
print()
print(stopwords)


"""
Save the words that are too low/high in frequency (stopwords).
"""
pkl_file = DATA_FOLDER+'learning/stop_words.p'
pickle.dump(stopwords, open(pkl_file, 'wb'))

pkl_file = DATA_FOLDER+'learning/inflections.p'
pickle.dump(inflections, open(pkl_file, 'wb'))

pkl_file = DATA_FOLDER+'learning/function_words.p'
pickle.dump(function_words, open(pkl_file, 'wb'))
