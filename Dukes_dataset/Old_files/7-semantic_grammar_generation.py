import numpy as np
import math as m
from constants import *
from xml_functions import *
from nltk.tree import *
from nltk.tree import ParentedTree
import pickle
import getpass
import operator
import itertools
from copy import deepcopy
import logging

logging.basicConfig(level=logging.INFO)
#--------------------------------------------------------------------------------------------------------#

def _read_tags():
    pkl_file = DATA_FOLDER+'learning/tags.p'
    data = open(pkl_file, 'rb')
    hypotheses_tags, VF_dict, LF_dict = pickle.load(data)
    return [hypotheses_tags, VF_dict, LF_dict]

def _read_sentences(scene):
    pkl_file = DATA_FOLDER+'scenes/'+str(scene)+'_sentences.p'
    data = open(pkl_file, 'rb')
    sentences = pickle.load(data)
    return sentences

def _read_vf(scene):
    pkl_file = DATA_FOLDER+'learning/'+str(scene)+'_visual_features.p'
    data = open(pkl_file, 'rb')
    vf,tree = pickle.load(data)
    return vf,tree

def _read_passed_tags():
    pkl_file = DATA_FOLDER+'matching/Passed_tags.p'
    data = open(pkl_file, 'rb')
    Matching,Matching_VF,passed_scenes,passed_ids = pickle.load(data)
    # print Matching,Matching_VF,passed_scenes,passed_ids
    return [Matching,Matching_VF,passed_scenes,passed_ids]

def _read_grammar_trees(scene):
    pkl_file = DATA_FOLDER+'learning/'+str(scene)+'_grammar.p'
    data = open(pkl_file, 'rb')
    tree = pickle.load(data)
    return tree

def _test_Actions(element):
    action_flag = 1
    action_position = []
    # print(element)
    # Get the position of every hypothesis tag representing an action.
    for count,item in enumerate(element):
        if 'actions_' in item:
            action_position.append(count)
    # print action_position
    # logging.debug(action_position)
    # Filter out hypotheses with no actions.
    if len(action_position) == 0:
        action_flag = 0
        # print 'test1'
    # If there is more than one action, check if the action positions are consecutive (no other elements in between).
    # If they do not have consecutive positions then we want to filter them out.

    # There was probably an assumption that each scene only depicts one action, but an action can be expressed by
    # more than one words?
    elif len(action_position) > 1:
        L = [a_i - b_i for a_i, b_i in zip(action_position[:-1],action_position[1:])]
        for i in L:
            if i!= -1:
                action_flag=0
                # print 'test2'
                # print action_position
    if action_flag:
        pass
        # print (element)
    return action_flag


"""
For every word in the subset (words contained in the grammar tree), find all hypothetical visual representations.
:return a list of lists of hypothetical visual representations, a dictionary with the subset words as keys and
their position in the sentence as values.
"""
#--------------------------------------------------------------------------------------------------------#
# this function tests one susbet of words at a time
def _all_possibilities_func(subset, hyp_language_pass):
    all_possibilities = []      # all the possibilities gathered in one list
    words = {}
    count = 0
    # Gather all possible visual representations of the every word in the subset.
    for word in subset:
        if word not in words:
            words[word] = count
            count += 1
            # Retrieve all possible visual representations of a word from the hypothesis dictionary.
            try:
                all_possibilities.append(hyp_language_pass[word].keys())
            except:
                print("word", word ,"is not a key")
    return all_possibilities,words


"""
Generate a vision tree corresponding to the given grammar tree.
"""
def _get_semantics(tree, hypotheses_tags):
    words = []

    # Get all words contained in the tree.
    # Loop through groups words that could represent action, entity and destination
    for i in range(len(tree)):
        # Loop through the words inside each group
        for word in tree[i]:
            words.append(word)

    # for i in range(len(tree)):
    #     words.extend(_get_all_ngrams(3, tree[i]))
    # print(words)
    semantic_trees = {}
    all_possibilities,words_dict = _all_possibilities_func(words, hypotheses_tags)

    # Iterate through the cartesian product of all_possibilities (list of sets containing visual hypotheses)
    # with itself, i.e. all possible ways of choosing one visual element hypothesis for each word in the sentence
    for count,element in enumerate(itertools.product(*all_possibilities)):
        # logging.debug(str(element) + "\n\n")
        # print element
        # Only keep hypotheses that suggest either exactly one action element or more consecutive action elements.
        valid = _test_Actions(element)
        if valid:
            logging.debug(tree)
            semantic_trees[count] = deepcopy(tree)
            c = 0
            for i in range(len(semantic_trees[count])):
                for j in range(len(semantic_trees[count][i])):
                    try:
                        semantic_trees[count][i][j] = element[words_dict[words[c]]]
                    except:
                        print("index out of range:", len(semantic_trees.keys()), count, i, j)
                    c += 1
    return semantic_trees

# Matching,Matching_VF,passed_scenes,passed_ids = _read_passed_tags()
# print Matching
hypotheses_tags, VF_dict, LF_dict = _read_tags()
# for word in Matching:
#     hypotheses_tags[word] = {}
#     for key in Matching[word]:
#         hypotheses_tags[word][key] = 0
#         # print hypotheses_tags[word]
#         # print Matching[word]
#         # print '----'


"""
Create semantic trees from every grammar tree.
Example:
    - grammar tree: (['lift'], ['yellow', cylinder])
    - semantic tree (['action_approach,grasp,lift'], ['color_yellow', 'type_cylinder'])
    - semantic tree is a visual! representation of the sentence based on the hypotheses tags created earlier.
"""

for scene in range(1,1001):
    semantic_trees = {}
    print('generating grammar from scene : ',scene)
    VF,Tree = _read_vf(scene)
    sentences = _read_sentences(scene)
    grammar_trees = _read_grammar_trees(scene)
    counter = 0
    # print ("sentences", sentences, "\n")
    # print ("grammar_trees", grammar_trees)
    # print ("--------------------------\n")

    # Loop through each sentence describing the scene.
    for id in sentences:
        # if id != 14789: continue
        # print '###############################################################'
        # for word in sentences[id]['text'].split(' ')
        # if 'tower' in sentences[id]['text']:
        #     if 'move' in sentences[id]['text']:
        # print id,sentences[id]['text']
        # print '###############################################################'
        semantic_trees[id] = {}
        # Loop through each grammar tree created from that sentence.
        for t in grammar_trees[id]:
            # if t != 7: continue
            tree = grammar_trees[id][t]
            # if ['move'] in tree and ['green', 'pyramid'] in tree:
            #     logging.basicConfig(level=logging.DEBUG)
            #     logging.debug('**************************************'+str(tree)+"\n---------------------------------------------------\n")
            # else:
            #     logging.basicConfig(level=logging.WARNING)
            semantic_trees[id][t] = _get_semantics(tree,hypotheses_tags)
            counter += len(semantic_trees[id][t])
            # if semantic_trees[id][t]:
            #     print (scene)
    pkl_file = DATA_FOLDER+'learning/'+str(scene)+'_semantic_grammar.p'
    pickle.dump(semantic_trees, open(pkl_file, 'wb'))
