import numpy as np
import math as m
from constants import * 
from xml_functions import *
from nltk.tree import *
from nltk.tree import ParentedTree
import pickle
import itertools
import getpass
import operator
#--------------------------------------------------------------------------------------------------------#
def _read_sentences(scene):
    pkl_file = DATA_FOLDER+'scenes/'+str(scene)+'_sentences.p'
    data = open(pkl_file, 'rb')
    sentences = pickle.load(data)
    return sentences

def _read_tfidf_words():
    pkl_file = DATA_FOLDER+'learning/idf_FW_linguistic_features.p'
    data = open(pkl_file, 'rb')
    tfidf = pickle.load(data)
    return tfidf

def _read_vf(scene):
    pkl_file = DATA_FOLDER+'learning/'+str(scene)+'_visual_features.p'
    data = open(pkl_file, 'rb')
    vf,tree = pickle.load(data)
    return vf,tree


"""
Find all possible grammar trees that could correspond to the given vision tree by slicing the 
sentence S into n slices, where n is the number of components in the vision tree. 
"""
def _get_grammar_trees(S,tree):
    grammar_trees = {}
    count = 0
    # print(len(S), S, tree)
    # print(tree['py'].keys(), len(tree['py'].keys()))
    try:
        # The tree has action, entity AND destination.
        if len(tree['py'].keys()) == 3:
            # Find all possible ways of splitting the sentence into 3 parts
            # (that will represent the action, entity and destination).
            for i1 in range(1,len(S)-1):
            # for i1 in range(1, 3):
                for i2 in range(1,len(S)-i1):
                    grammar_trees[count] = [S[0:i1],S[i1:i2+i1],S[i2+i1:]]
                    count+=1
    except:
        pass
    try:
        # The tree has only action and entity.
        if len(tree['py'].keys()) == 2:
            # Find all possible ways of splitting the sentence into 2 parts
            # (that will represent the action and entity).
            for i1 in range(1,len(S)):
                grammar_trees[count] = [S[0:i1],S[i1:]]
                count+=1
    except:
        pass
    # print(grammar_trees)

    # This should be done with every sentence, unfortunately, the computational cost is too high.
    if len(S) > 0:
        return grammar_trees

    seriously_all_grammar_trees = []
    for tree_index in grammar_trees:
        ngram_trees = get_grammar_trees_with_ngrams(grammar_trees[tree_index])
        seriously_all_grammar_trees.extend(ngram_trees)

    all_trees_dict = {}
    for count, tree in enumerate(seriously_all_grammar_trees):
        all_trees_dict[count] = tree


    return  all_trees_dict #grammar_trees

def get_grammar_trees_with_ngrams(grammar_tree):
    trees = []
    leaf_options = {}
    for index, leaf in enumerate(grammar_tree):
        split_options = []
        leaf_options[index] = []
        upper_bound = len(leaf)
        superset = list(range(1,upper_bound))
        min_splits = int(upper_bound/3)
        split_options = (itertools.chain.from_iterable(itertools.combinations(superset, r) for r in range(min_splits, len(superset) + 1)))
        for option in split_options:
            new_leaf = []
            last_edge = 0
            for edge in option:
                new_leaf.append(' '.join(leaf[last_edge:edge]))
                last_edge = edge
            new_leaf.append(' '.join(leaf[last_edge:upper_bound]))
            # print(new_leaf)
            leaf_options[index].append(new_leaf)

    if len(grammar_tree) == 3:
        all_trees = (list(itertools.product(leaf_options[0], leaf_options[1], leaf_options[2])))
        all_trees = [list(tree) for tree in all_trees]

    if len(grammar_tree) == 2:
        all_trees = (list(itertools.product(leaf_options[0], leaf_options[1])))
        all_trees = [list(tree) for tree in all_trees]

    # for tree in all_trees:
    #     print(tree)

    return all_trees


# pkl_file = '/home/mo/Datasets/11-Leeds/Dukes_modified/learning/tags.p'
# data = open(pkl_file, 'rb')
# hypotheses_tags, VF_dict, LF_dict = pickle.load(data)
# this is why I can't have nice things
# total = 1
# for word in hypotheses_tags:
#     total*=len(hypotheses_tags[word].keys())+1
# print '>>>>>>>>>>>>>>>>>>',total
# In this dataset, we have 114 unique words, were each word has between 2 and 4 potential visual tags. If we compute the combinations of these words tags it's is 1.7 quattuordecillion 1.7*10^45, which gives the reader an idea of how massive the search space is, and that it can't be the case where we keep track of all combinations. Therefore we take a different approach, were we do the learning incrementally. The system analyse each snetnece seperatly, and record

tfidf_words = _read_tfidf_words()

for scene in range(1,1001):
    print ('generating grammar from scene : ',scene)
    VF,Tree = _read_vf(scene)
    sentences = _read_sentences(scene)
    grammar_trees = {}
    for id in sentences:
        # Get the list of words in the sentence.
        S = sentences[id]['text'].split(' ')
        # Remove stopwords.
        for word in tfidf_words:
            S = list(filter(lambda a: a != word, S))
        # Find all possible grammar trees that could correspond to the vision tree.
        grammar_trees[id] = _get_grammar_trees(S,Tree)
    if scene == 1:
        print(grammar_trees)
    pkl_file = DATA_FOLDER+'learning/'+str(scene)+'_grammar.p'
    # print('\n\n\n', grammar_trees, '\n\n\n')
    pickle.dump(grammar_trees, open(pkl_file, 'wb'))
